﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectManagementLayer.Models;

namespace ProjectManagementLayer.ViewModels
{
    public class ProjectFormViewModel
    {
        public ProjectModel ProjectModel { get; set; }
        public List<GroupModel> GroupModel { get; set; }
    }
}