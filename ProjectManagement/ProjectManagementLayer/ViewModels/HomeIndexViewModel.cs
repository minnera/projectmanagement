﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectManagementLayer.Models;

namespace ProjectManagementLayer.ViewModels
{
    public class HomeIndexViewModel
    {
        public List<ProjectModel> Projects { get; set; }
        public List<TaskModel> Tasks { get; set; }
        public UserModel User { get; set; }
        public string UserName { get; set; }
    }
}