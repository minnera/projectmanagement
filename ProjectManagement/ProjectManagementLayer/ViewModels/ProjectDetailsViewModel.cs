﻿using ProjectManagementLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.ViewModels
{
    public class ProjectDetailsViewModel
    {
        public ProjectModel ProjectModel { get; set; }
        public List<TaskModel> TaskModels { get; set; }
        public GroupModel GroupModel { get; set; }
    }
}