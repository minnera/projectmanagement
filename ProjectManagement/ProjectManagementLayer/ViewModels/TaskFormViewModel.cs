﻿using ProjectManagementLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.ViewModels
{
    public class TaskFormViewModel
    {
        public TaskModel TaskModel { get; set; }

        public List<TaskModel> TaskList { get; set; }
        public List<ProjectModel> ProjectModels { get; set; }
    }
}