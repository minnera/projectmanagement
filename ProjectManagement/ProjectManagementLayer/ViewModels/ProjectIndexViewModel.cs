﻿using ProjectManagementLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.ViewModels
{
    public class ProjectIndexViewModel
    {
        private List<ProjectModel> userAddedProject;
        public List<ProjectModel> AllProjects { get; set; }
        public List<ProjectModel> UserAddedProjects { get { return userAddedProject; } }

        public void setUserAddedProjects(ProjectModel userChoice)
        {

            if (userAddedProject == null)
                userAddedProject = new List<ProjectModel>();
            if(userChoice != null)
                UserAddedProjects.Add(userChoice);
        }
    }
}