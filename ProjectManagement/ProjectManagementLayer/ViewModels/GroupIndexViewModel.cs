﻿using ProjectManagementLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.ViewModels
{
    public class GroupIndexViewModel
    {
        public List<UserModel> UserModels { get; set; }
        public List<GroupModel> GroupModels { get; set; }
        public List<UserGroupModel> UserGroupModels { get; set; }
        public List<ProjectModel> ProjectModels { get; set; }
    }
}