﻿using ProjectManagementLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.ViewModels
{
    public class TaskDetailsViewModel
    {
        public TaskModel TaskModel { get; set; }
        public TaskModel RequiredTask { get; set; }
    }
}