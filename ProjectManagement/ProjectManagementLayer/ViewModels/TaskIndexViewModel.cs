﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.ViewModels
{
    public class TaskIndexViewModel
    {
        public int ProjectId { get; set; }
        public Models.UserModel UserModel { get; set; }
    }
}