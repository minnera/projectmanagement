﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.Models
{
    public class GroupModel
    {
        [Display(Name = "Group ID")]
        [Required]
        public int GId { get; set; }

        [Display(Name = "Group Name")]
        [Required]
        [StringLength(50, MinimumLength = 5)]
        public string GName { get; set; }

        [StringLength(25)]
        public string GVisibility { get; set; }

        [Display(Name = "Group created date")]
        public DateTime GDate { get; set; }

        [Display(Name = "Group Properties")]
        [StringLength(25)]
        [Required]
        public string GType { get; set; }

        [Display(Name = "Group Wage")]
        public int GWage { get; set; }

        [Display(Name = "Group Last Modified Date")]
        public DateTime GLastChange { get; set; }
    }
}