﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.Models
{
    public class UserModel
    {
        [Display(Name = "User ID")]
        [Required]
        public int UId { get; set; }

        [Display(Name = "Login Name")]
        [Required]
        public string UName { get; set; }

        [Display(Name = "Password")]
        [Required]
        public string UPassword { get; set; }

        [Display(Name = "Gender")]
        [Required]
        public string UGender { get; set; }

        [Display(Name = "Day of Birth")]
        [Required]
        public DateTime UBdate { get; set; }

        [Display(Name = "Annual Salary")]
        [Required]
        public float UTotalSalary { get; set; }
    }
}