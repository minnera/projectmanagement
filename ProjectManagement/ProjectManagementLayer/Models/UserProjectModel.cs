﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.Models
{
    public class UserProjectModel
    {
        [Required]
        public int UPId { get; set; }
        public int UId { get; set; }
        public int PId { get; set; }
    }
}