﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.Models
{
    public class TaskModel
    {
        [Display(Name = "Task ID")]
        [Required]
        public int TId { get; set; }

        [Display(Name = "Task Name")]
        [Required]
        public string TName { get; set; }

        [Display(Name = "Is task finished?")]
        [Required]
        public bool TIsDone { get; set; }

        [Display(Name = "Is task archived?")]
        [Required]
        public bool TIsArchived { get; set; }

        [Display(Name = "Task Details")]
        [Required]
        public string TComment { get; set; }

        [Display(Name = "Task Requirement")]
        public int? TRequirement { get; set; }

        [Display(Name = "Which project would you assigned?")]
        public int T_PId { get; set; }
        public int T_UId { get; set; }
    }
}