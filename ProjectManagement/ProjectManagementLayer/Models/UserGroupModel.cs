﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.Models
{
    public class UserGroupModel
    {
        [Required]
        public int UGId { get; set; }
        public int UId { get; set; }
        public int GId { get; set; }
        public string UGPost { get; set; }

    }
}