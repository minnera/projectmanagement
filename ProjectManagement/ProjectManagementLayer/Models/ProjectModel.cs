﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.Models
{
    public class ProjectModel
    {
        [Display(Name = "Project ID")]
        [Required]
        public int PId { get; set; }

        [Display(Name = "Project Name")]
        [Required]
        [StringLength(30, MinimumLength = 5)]
        public string PName { get; set; }

        [Display(Name = "Project Comment")]
        [Required(ErrorMessage = "Please enter project's name.")]
        [StringLength(255, MinimumLength = 10)]
        public string PComment { get; set; }

        [Display(Name = "Project Priority")]
        [Range(1,10)]
        [Required]
        public int PPriority { get; set; }

        public bool PIsArchived { get; set; }

        [Display(Name = "Which group attended?")]
        [Required]
        public int P_GId_Maker { get; set; }

        [Display(Name = "Deadline")]
        [Required]
        public DateTime PDeadline { get; set; }

        [Display(Name = "Last Modified")]
        public DateTime PLastModify { get; set; }

        public DateTime PRecorded { get; set; }
    }
}