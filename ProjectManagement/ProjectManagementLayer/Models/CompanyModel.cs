﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.Models
{
    public class CompanyModel
    {
        [Display(Name = "Company ID")]
        [Required]
        public int CId { get; set; }

        [Display(Name = "Company Name")]
        [Required]
        public string CName { get; set; }

        [Display(Name = "Company Password")]
        [Required]
        public int CPassword { get; set; }

        [Display(Name = "Company established date")]
        public DateTime CDate { get; set; }

        [Display(Name = "Company Full Name")]
        public string CFullName { get; set; }

        [Display(Name = "Company fields")]
        [Required]
        public string CIndustry { get; set; }

        [Display(Name = "Company Price")]
        public float CPrice { get; set; }
    }
}