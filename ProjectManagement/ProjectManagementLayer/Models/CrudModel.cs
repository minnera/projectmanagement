﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.Models
{
    public class CrudModel
    {
        public ProjectModel EditObject { get; set; }
        public List<ProjectModel> ProjectList { get; set; }
    }
}