﻿using AutoMapper;
using BusinessLogic;
using ProjectManagementLayer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace ProjectManagementLayer.Controllers.Api
{
    //nézd meg hogy honnan autentikál az identity
    public class UsersController : ApiController
    {
        private ILogic _context;
        private IMapper mapper;

        public UsersController()
        {
            _context = new RealLogic();
            mapper = DTO.AutoMapperConfig.GetMapper();
        }
        // GET /api/users
        public IEnumerable<UserDto> GetUsers()
        {
            var userlist = _context.GetUsers().ToList();
            return mapper.Map<List<Data.USERS>,List<DTO.UserDto>>(userlist);
        }

        // GET /api/users/1
        public IHttpActionResult GetUser(int id)
        {
            var user = _context.GetOneUser(id);

            if (user == null)
                return NotFound();

            return Ok(mapper.Map<Data.USERS, UserDto>(user));
        }

        // POST /api/users
        [HttpPost]
        public IHttpActionResult CreateUser(UserDto userDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var user = mapper.Map<UserDto, Data.USERS>(userDto);
            _context.AddUser(user);

            userDto.UId = user.UID;
            return Created(new Uri(Request.RequestUri + "/" + user.UID), userDto);
        }

        // PUT /api/users/1
        [HttpPut]
        public void UpdateUser(int id, UserDto userDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var userInDb = _context.GetOneUser(id);

            if (userInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            mapper.Map(userDto, userInDb);
            //kell egy olyan ilogic ami modositáskor egész projectet fogad 
            _context.ModifyUser(userInDb.UID, userInDb.UNAME, userInDb.UPASSWORD, userInDb.UGENDER, (DateTime)userInDb.UBDATE, userInDb.UFULLNAME);
        }

        // DELETE /api/users/1
        [HttpDelete]
        public IHttpActionResult DeleteUser(int id)
        {
            var userInDb = _context.GetOneUser(id);

            if (userInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.DelProject(id);
            return Ok("ok");
        }
    }
}
