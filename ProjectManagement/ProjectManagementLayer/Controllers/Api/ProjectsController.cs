﻿using AutoMapper;
using BusinessLogic;
using ProjectManagementLayer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace ProjectManagementLayer.Controllers.Api
{
    public class ProjectsController : ApiController
    {
        private ILogic _context;
        private IMapper mapper;

        public ProjectsController()
        {
            _context = new RealLogic();
            mapper = DTO.AutoMapperConfig.GetMapper();
        }
        // GET /api/projects
        public IEnumerable<ProjectDto> GetProjects()
        {
            return _context.GetProjects().ToList().Select(mapper.Map<Data.PROJECTS, DTO.ProjectDto>);
        }

        // GET /api/projects/1
        public IHttpActionResult GetProject(int id)
        {
            var project = _context.GetOneProject(id);

            if (project == null)
                return NotFound();

            return Ok(mapper.Map<Data.PROJECTS, ProjectDto>(project));
        }

        // POST /api/projects
        [HttpPost]
        public IHttpActionResult CreateProject(ProjectDto projectDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var project = mapper.Map<ProjectDto, Data.PROJECTS>(projectDto);
            _context.AddProject(project);

            projectDto.PId = project.PID;
            return Created(new Uri(Request.RequestUri + "/" + project.PID), projectDto);
        }

        // PUT /api/projects/1
        [HttpPut]
        public void UpdateProject(int id, ProjectDto projectDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var projectInDb = _context.GetOneProject(id);

            if (projectInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            mapper.Map(projectDto, projectInDb);
            //kell egy olyan ilogic ami modositáskor egész projectet fogad 
            _context.ModifyProject(projectInDb.PID,projectInDb.PNAME, projectInDb.PISARCHIVED, projectInDb.PISPINNED, projectInDb.PISDONE, (DateTime)projectInDb.PDEADLINE, projectInDb.PPRIORITY, projectInDb.PCOMMENT);
        }

        // DELETE /api/projects/1
        [Authorize(Roles = "Manager")]
        [HttpDelete]
        public IHttpActionResult DeleteProject(int id)
        {
            var projectInDb = _context.GetOneProject(id);

            if (projectInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            var taskList = _context.GetTasks().Where(t => t.T_PID == id && t.T_PID == id).ToList();

            foreach (var task in taskList)
            {
                _context.DelTask(task.TID);
            }
            _context.DelProject(id);
            return Ok("ok");
        }

        [HttpGet]
        public IHttpActionResult AddProjectToUser(int id, string name)
        {
            //var userName = IdentityExtensions.GetUserName(User.Identity);
            //var userName = RequestContext.Principal.Identity.Name;

            var projectList = _context.GetOneProject(id);
            var userNameInDb = _context.GetUsers().Single(u => u.UNAME == name).UID;
            var groupConnection = _context.GetUserGroupConnections().SingleOrDefault(c => c.GID == projectList.P_GID_MAKER && c.UID == userNameInDb);
            if (groupConnection != null)
            {
                var userProjectList = _context.GetUserProjectsConnections().Select(mapper.Map<Data.UPCONNT, Models.UserProjectModel>).ToList();
                bool isNotAddedYet = true;
                foreach (var connection in userProjectList)
                {
                    if (connection.PId == id && connection.UId == _context.GetUsers().Single(u => u.UNAME == name).UID)
                    {
                        isNotAddedYet = false;
                    }
                }
                if (isNotAddedYet)
                {
                    var userName = name;
                    var userProjectModel = new Models.UserProjectModel();
                    userProjectModel.UPId = _context.GetNextUserProjectConnectionId();
                    userProjectModel.PId = id;
                    userProjectModel.UId = _context.GetUsers().Single(u => u.UNAME == userName).UID;
                    _context.AddUserProjectConnection(mapper.Map<Models.UserProjectModel, Data.UPCONNT>(userProjectModel));

                    return Ok("A második teszt lépett életbe. SIKER!!!!");
                }

                return BadRequest("Létezik ilyen kapcsolat már");
            }

            return BadRequest("Nem vagy tagja a groupnak!");
        }

        [HttpDelete]
        
        public IHttpActionResult DeleteUserProjectConnection(int id, string name)
        {

            var user = _context.GetUsers().Single(u => u.UNAME == name);
            var upconnect = _context.GetUserProjectsConnections().Single(up => up.PID == id && up.UID == user.UID);

            var taskList = _context.GetTasks().Where(t => t.T_UID == user.UID && t.T_PID == id).ToList();
            foreach (var task in taskList)
            {
                _context.DelTask(task.TID);
            }
            _context.DelUPConnection(upconnect);
            return Ok("Törlés sikeres.");
        }
    }
}
