﻿using AutoMapper;
using BusinessLogic;
using ProjectManagementLayer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjectManagementLayer.Controllers.Api
{
    public class TasksController : ApiController
    {
        private IMapper mapper;
        private ILogic _context;

        public TasksController()
        {
            mapper = DTO.AutoMapperConfig.GetMapper();
            _context = new RealLogic();
        }

        //GET /api/tasks
        [HttpGet]
        public IEnumerable<TaskDto> GetTasks()
        {
            return _context.GetTasks().ToList().Select(mapper.Map<Data.TASKS, DTO.TaskDto>);
        }

        [Route("api/tasks/admin")]
        [HttpGet]
        public IEnumerable<TaskDto> GetAdminTasks()
        {
            return _context.GetTasks().Where(t => t.TISDONE == false).Select(mapper.Map<Data.TASKS, DTO.TaskDto>).ToList();
        }

        [Route("api/tasks/gettask/{id}")]
        // GET /api/tasks/1
        public IHttpActionResult GetTask(int id)
        {
            var task = _context.GetOneTask(id);

            if (task == null)
                return NotFound();

            return Ok(mapper.Map<Data.TASKS, TaskDto>(task));
        }

        [HttpGet]
        public IEnumerable<TaskDto> GetTasksForProject(int id)
        {
            var tasks = _context.GetTasks().Where(task => task.T_PID == id  && task.TISDONE == false).Select(mapper.Map<Data.TASKS, DTO.TaskDto>);

            return tasks;
        }

        // POST /api/tasks
        [HttpPost]
        public IHttpActionResult CreateTask(TaskDto taskDto)
        {
            //addolni a getnextid() metódust?
            if (!ModelState.IsValid)
                return BadRequest();

            var task = mapper.Map<TaskDto, Data.TASKS>(taskDto);
            _context.AddTask(task);

            taskDto.TId = task.TID;
            return Created(new Uri(Request.RequestUri + "/" + task.TID), taskDto);
        }

        // PUT /api/tasks/1
        [HttpPut]
        public void UpdateTask(int id, TaskDto taskDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var taskInDb = _context.GetOneTask(id);

            if (taskInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            mapper.Map(taskDto, taskInDb);
            //kell egy olyan ilogic ami modositáskor egész projectet fogad 
            _context.ModifyTask(taskInDb.TID, taskInDb.TNAME, taskInDb.TISDONE, taskInDb.TISARCHIVED, taskInDb.TISPINNED, (float)taskInDb.TEXPENSES, taskInDb.TCOMMENT, (int)taskInDb.TREQUIREMENT);
        }

        [Route("api/tasks/modifyuser")]
        [HttpPut]
        public void UpdateUserForTask(TaskDto taskDto)
        {
            var taskInDb = _context.GetOneTask(taskDto.TId);

            if (taskInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.ModifyUserIDInTask(taskDto.TId, taskDto.T_UId);
        }

        // DELETE /api/projects/1
        [HttpDelete]
        public IHttpActionResult DeleteTask(int id)
        {
            var taskInDb = _context.GetOneTask(id);

            if (taskInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.DelTask(id);
            return Ok("ok");
        }

        //[Route("api/tasks/isdone/{id}/{name}")]
        [HttpGet]
        public IHttpActionResult AddTaskToDone(int id, string name)
        {
            var userId = _context.GetUsers().Single(u => u.UNAME == name).UID;
            var taskList = _context.GetTasks().Where(t => t.TISDONE == true && t.T_UID == userId).Select(mapper.Map<Data.TASKS, DTO.TaskDto>).ToList();
            return Ok(taskList);
        }

        [Route("api/tasks/isdone/{id}")]
        [HttpGet]
        public IHttpActionResult AddTaskToDone(int id)
        {
            var taskInDb = _context.GetOneTask(id);
            if (taskInDb.TISDONE == true)
                return BadRequest("Már hozzá volt adva!");
            if(taskInDb.TREQUIREMENT.HasValue)
            {
                var taskReq = _context.GetOneTask(taskInDb.TREQUIREMENT.Value);
                if (taskReq.TISDONE == false)
                    return BadRequest("Nincs kész az előfeltétele!");
            }
            taskInDb.TISDONE = true;
            _context.ModifyTasK(taskInDb);
            return Ok("Sikeres módositás!");
        }
    }
}
