﻿using AutoMapper;
using BusinessLogic;
using ProjectManagementLayer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjectManagementLayer.Controllers.Api
{
    public class GroupsController : ApiController
    {
        private ILogic _context;
        private IMapper mapper;

        public GroupsController()
        {
            _context = new RealLogic();
            mapper = DTO.AutoMapperConfig.GetMapper();
        }
        // GET /api/groups
        public IEnumerable<GroupDto> GetGroups()
        {
            return _context.GetGroups().ToList().Select(mapper.Map<Data.GROUPS, DTO.GroupDto>);
        }
        
        // GET /api/groups/1
        public IHttpActionResult GetGroups(int id)
        {
            var group = _context.GetOneGroup(id);

            if (group == null)
                return NotFound();

            return Ok(mapper.Map<Data.GROUPS, GroupDto>(group));
        }

        // POST /api/groups
        [HttpPost]
        public IHttpActionResult CreateGroup(GroupDto groupDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var group = mapper.Map<GroupDto, Data.GROUPS>(groupDto);
            _context.AddGroup(group);

            groupDto.GId = group.GID;
            return Created(new Uri(Request.RequestUri + "/" + group.GID), groupDto);
        }

        // PUT /api/groups/1
        [HttpPut]
        public void UpdateGroup(int id, GroupDto groupDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var groupInDb = _context.GetOneGroup(id);

            if (groupInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            mapper.Map(groupDto, groupInDb);
            //kell egy olyan ilogic ami modositáskor egész groupot fogad 
            _context.ModifyGroup(groupInDb.GID, groupInDb.GNAME, groupInDb.GTYPE, (float)groupInDb.GWAGE);
        }

        // DELETE /api/groups/1
        [Authorize(Roles = "Manager")]
        [HttpDelete]
        public IHttpActionResult DeleteGroup(int id)
        {
            var groupInDb = _context.GetOneGroup(id);

            if (groupInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.DelGroup(id);
            return Ok("ok");
        }

        [HttpGet]
        public IHttpActionResult JoinGroup(int id, string name)
        {
            var userGroupList = _context.GetUserGroupConnections().Select(mapper.Map<Data.UGCONNT, Models.UserGroupModel>).ToList();
            bool isNotAddedYet = true;
            foreach (var connection in userGroupList)
            {
                if (connection.GId == id && connection.UId == _context.GetUsers().Single(u => u.UNAME == name).UID)
                {
                    isNotAddedYet = false;
                }
            }
            if (isNotAddedYet)
            {
                var userName = name;
                var userGroupModel = new Models.UserGroupModel();
                userGroupModel.UGId = _context.GetNextUserGroupConnectionId();
                userGroupModel.GId = id;
                userGroupModel.UId = _context.GetUsers().Single(u => u.UNAME == userName).UID;
                userGroupModel.UGPost = "";
                _context.AddUserGroupConnection(mapper.Map<Models.UserGroupModel, Data.UGCONNT>(userGroupModel));

                return Ok("A második teszt lépett életbe. SIKER!!!!");
            }

            return BadRequest("Létezik ilyen kapcsolat már");
        }
    }
}
