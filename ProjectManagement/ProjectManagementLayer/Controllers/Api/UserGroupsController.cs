﻿using AutoMapper;
using BusinessLogic;
using ProjectManagementLayer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjectManagementLayer.Controllers.Api
{
    public class UserGroupsController : ApiController
    {
        private ILogic _context;
        private IMapper mapper;

        public UserGroupsController()
        {
            _context = new FakeLogic();
            mapper = DTO.AutoMapperConfig.GetMapper();
        }
        // GET /api/usergroups
        public IEnumerable<UserGroupDto> GetUserGroups()
        {
            var userGroup = _context.GetUserGroupConnections().ToList().Select(mapper.Map<Data.UGCONNT, DTO.UserGroupDto>);
            return userGroup;
        }

        // GET /api/usergroups/1
        public IHttpActionResult GetUserGroup(int id)
        {
            var userGroup= _context.GetOneUserGroupConnection(id);

            if (userGroup == null)
                return NotFound();

            return Ok(mapper.Map<Data.UGCONNT, UserGroupDto>(userGroup));
        }

        // POST /api/usergroups
        [HttpPost]
        public IHttpActionResult CreateUserGroups(UserGroupDto userGroupsDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var userGroups = mapper.Map<UserGroupDto, Data.UGCONNT>(userGroupsDto);
            _context.AddUserGroupConnection(userGroups);

            userGroupsDto.UGId = userGroups.UGID;
            return Created(new Uri(Request.RequestUri + "/" + userGroups.UGID), userGroupsDto);
        }

        // PUT /api/usergroups/1
        [HttpPut]
        public void UpdateUserGroup(int id, UserGroupDto UserGroupDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var userGroupInDb = _context.GetOneUserGroupConnection(id);

            if (userGroupInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            mapper.Map(UserGroupDto, userGroupInDb);
            //Nincs módositás, csak törlés és újra generálás
        }

        // DELETE /api/projects/1
        [HttpDelete]
        public IHttpActionResult DeleteUserGroup(int id)
        {
            var userGroupInDb = _context.GetOneUserGroupConnection(id);

            if (userGroupInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.DelProject(id);
            return Ok("ok");
        }
    }
}
