﻿using AutoMapper;
using BusinessLogic;
using ProjectManagementLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagementLayer.Controllers
{
    public class GroupController : Controller
    {
        IMapper mapper;
        ILogic _context;
        public GroupController()
        {
            mapper = DTO.AutoMapperConfig.GetMapper();
            _context = new RealLogic();
        }

        // GET: Index
        public ActionResult Index()
        {
            var groups = _context.GetGroups().Select(mapper.Map<Data.GROUPS, Models.GroupModel >).ToList();
            var users = _context.GetUsers().Select(mapper.Map<Data.USERS, Models.UserModel>).ToList();
            var userList = _context.GetUserGroupConnections().Select(mapper.Map<Data.UGCONNT, Models.UserGroupModel>).ToList();
            var projectList = _context.GetProjects().Select(mapper.Map<Data.PROJECTS, Models.ProjectModel>).ToList();
            var viewModel = new GroupIndexViewModel()
            {
                GroupModels = groups,
                UserModels = users,
                UserGroupModels = userList,
                ProjectModels = projectList
                
            };
            return View(viewModel);
        }

        public ActionResult New()
        {
            ViewBag.Name = "Create new Group";
            var group = new Models.GroupModel();
            
            return View("GroupForm", group);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Models.GroupModel groupModel)
        {
            if (!ModelState.IsValid)
            {
                return View("GroupForm", groupModel);
            }
            if (groupModel.GId == 0)
            {
                groupModel.GId = _context.GetNextGroupId();
                groupModel.GWage = 0;
                groupModel.GDate = System.DateTime.Now;
                groupModel.GLastChange = DateTime.Now;
                groupModel.GVisibility = "sd";
                var group = mapper.Map<Models.GroupModel, Data.GROUPS>(groupModel);
                _context.AddGroup(group);
                var userGroupConnection = new Models.UserGroupModel()
                {
                    UGId = _context.GetNextUserGroupConnectionId(),
                    GId = groupModel.GId,
                    UId = _context.GetUsers().Single(u => u.UNAME == User.Identity.Name).UID,
                    UGPost = ""
                };
                _context.AddUserGroupConnection(mapper.Map<Models.UserGroupModel, Data.UGCONNT>(userGroupConnection));
            }
            else
            {
                var groupInDb = _context.GetGroups().Single(g => g.GID == groupModel.GId);
                mapper.Map(groupModel, groupInDb);
                _context.AddGroup(groupInDb);
            }

            return RedirectToAction("Index", "Group");
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Name = "Edit Group";
            var group = _context.GetGroups().SingleOrDefault(g => g.GID == id);

            if (group == null)
                return HttpNotFound();
            
            return View("GroupForm", group);
        }
    }
}