﻿using AutoMapper;
using BusinessLogic;
using ProjectManagementLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagementLayer.Controllers
{
    public class CrudController : Controller
    {
        ILogic logic;
        IMapper mapper;
        CrudModel localModel;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            mapper = DTO.AutoMapperConfig.GetMapper();
            logic = new FakeLogic();

            IQueryable<Data.PROJECTS> projects = logic.GetProjects();
            localModel = new CrudModel();
            localModel.EditObject = new ProjectModel();
            localModel.ProjectList = mapper.Map<IQueryable<Data.PROJECTS>, List<ProjectModel>>(projects);

            base.OnActionExecuting(filterContext);
        }
    }
}