﻿using AutoMapper;
using BusinessLogic;
using ProjectManagementLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagementLayer.Controllers
{
    public class ProjectController : Controller
    {
        private ILogic _context;
        private IMapper mapper;
        public ProjectController()
        {
            _context = new RealLogic();
            mapper = DTO.AutoMapperConfig.GetMapper();
        }

        public ActionResult Index()
        {
            var userInDb = _context.GetUsers().Single(user => user.UNAME == User.Identity.Name);
            var projects = _context.GetUsersProjectsUnderProcess(userInDb.UID).ToList();
            var projectsModel = mapper.Map<List<Data.PROJECTS>, List<Models.ProjectModel>>(projects);
            return View(projectsModel);
        }

        public ActionResult AddProject()
        {
            var projects = _context.GetProjects().ToList();
            var projectsModel = mapper.Map<List<Data.PROJECTS>, List<Models.ProjectModel>>(projects);
            return View(projectsModel);
        }

        public ActionResult Details(int id)
        {
            var project = _context.GetOneProject(id);
            var projectModel = mapper.Map<Data.PROJECTS, Models.ProjectModel>(project);
            var group = _context.GetOneGroup(project.P_GID_MAKER);
            var groupModel = mapper.Map<Data.GROUPS, Models.GroupModel>(group);
            var tasks = _context.GetTasks().Where(task => task.T_PID == project.PID).ToList();
            var taskModel = mapper.Map<List<Data.TASKS>, List<Models.TaskModel>>(tasks);

            var viewModel = new ProjectDetailsViewModel
            {
                ProjectModel = projectModel,
                GroupModel = groupModel,
                TaskModels = taskModel
            };

            if (viewModel == null)
                return HttpNotFound();

            return View(viewModel);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult New()
        {
            ViewBag.Name = "Create new Project";
            var groups = _context.GetGroups().ToList();
            //var groupsList = mapper.Map<List<Data.GROUPS>, List<DTO.Group>>(groups);
            var newProjectViewModel = new ProjectFormViewModel
            {
                ProjectModel = new Models.ProjectModel(),
                GroupModel = mapper.Map<List<Data.GROUPS>, List<Models.GroupModel>>(groups)
            };
            return View("ProjectForm",newProjectViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Models.ProjectModel projectModel)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new ProjectFormViewModel
                {
                    ProjectModel = projectModel,
                    GroupModel = mapper.Map<List<Data.GROUPS>, List<Models.GroupModel>>(_context.GetGroups().ToList())
                };
                return View("ProjectForm", viewModel);
            }
            if(projectModel.PId == 0)
            {
                projectModel.PId = _context.GetNextProjectId();
                projectModel.PLastModify = DateTime.Now;
                projectModel.PRecorded = DateTime.Now;
                var project = mapper.Map<Models.ProjectModel, Data.PROJECTS>(projectModel);
                _context.AddProject(project);
            }
            else
            {
                var projectInDb = _context.GetProjects().Single(c => c.PID == projectModel.PId);
                mapper.Map(projectModel, projectInDb);
                _context.AddProject(projectInDb);
                //_context.ModifyProject(projectModel.PId, projectModel.PName, projectModel.PIsArchived, true, false, projectModel.PDeadline, projectModel.PPriority, projectModel.PComment);
            }
        
            return RedirectToAction("Index", "Project");
        }

        public ActionResult Edit(int id)
        {
            if (User.IsInRole("Manager"))
            {
                ViewBag.Name = "Edit Project";
                var project = _context.GetProjects().SingleOrDefault(c => c.PID == id);

                if (project == null)
                    return HttpNotFound();
                var group = _context.GetGroups().ToList();
                var viewModel = new ProjectFormViewModel
                {
                    ProjectModel = mapper.Map<Data.PROJECTS, Models.ProjectModel>(project),
                    GroupModel = mapper.Map<List<Data.GROUPS>, List<Models.GroupModel>>(group)
                };
                return View("ProjectForm", viewModel);
            }
            else
            {
                var project = _context.GetOneProject(id);
                var projectModel = mapper.Map<Data.PROJECTS, Models.ProjectModel>(project);
                var group = _context.GetOneGroup(project.P_GID_MAKER);
                var groupModel = mapper.Map<Data.GROUPS, Models.GroupModel>(group);
                var tasks = _context.GetTasks().Where(task => task.T_PID == project.PID).ToList();
                var taskModel = mapper.Map<List<Data.TASKS>, List<Models.TaskModel>>(tasks);

                var viewModel = new ProjectDetailsViewModel
                {
                    ProjectModel = projectModel,
                    GroupModel = groupModel,
                    TaskModels = taskModel
                };

                if (viewModel == null)
                    return HttpNotFound();

                return View("Details", viewModel);
            }
        }
    }
}