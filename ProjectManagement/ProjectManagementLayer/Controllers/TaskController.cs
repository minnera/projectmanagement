﻿using AutoMapper;
using BusinessLogic;
using ProjectManagementLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagementLayer.Controllers
{
    public class TaskController : Controller
    {
        private IMapper mapper;
        private ILogic _context;

        public TaskController()
        {
            mapper = DTO.AutoMapperConfig.GetMapper();
            _context = new RealLogic();
        }

        public ActionResult Index(int id)
        {
            var viewModel = new TaskIndexViewModel
            {
                ProjectId = id,
                UserModel = _context.GetUsers().Select(mapper.Map<Data.USERS, Models.UserModel>).Single(u => u.UName == User.Identity.Name)
            };
            return View(viewModel);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Admin()
        {
            var users = _context.GetUsers().Select(mapper.Map<Data.USERS, Models.UserModel>).ToList();
            return View(users);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Name = "Edit Task";

            var task = _context.GetOneTask(id);

            if (task == null)
                return HttpNotFound();
            var projects = _context.GetProjects().ToList();
            var taskList = _context.GetTasks().Select(mapper.Map<Data.TASKS, Models.TaskModel>).ToList();
            var viewModel = new TaskFormViewModel
            {
                TaskModel = mapper.Map<Data.TASKS, Models.TaskModel>(task),
                ProjectModels = mapper.Map<List<Data.PROJECTS>, List<Models.ProjectModel>>(projects),
                TaskList = taskList
            };
            return View("TaskForm", viewModel);
        }

        public ActionResult New(int id)
        {
            ViewBag.Name = "Create new Task";
            var projects = _context.GetProjects().ToList();
            var taskList = _context.GetTasks().ToList();
            //var groupsList = mapper.Map<List<Data.GROUPS>, List<DTO.Group>>(groups);
            var newTaskViewModel = new TaskFormViewModel
            {
                TaskModel = new Models.TaskModel() { T_PId = id},
                ProjectModels = mapper.Map<List<Data.PROJECTS>, List<Models.ProjectModel>>(projects),
                TaskList = mapper.Map<List<Data.TASKS>, List<Models.TaskModel>>(taskList)
            };
            return View("TaskForm", newTaskViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Models.TaskModel taskModel)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new TaskFormViewModel
                {
                    TaskModel = taskModel,
                    ProjectModels = mapper.Map<List<Data.PROJECTS>, List<Models.ProjectModel>>(_context.GetProjects().ToList()),
                    TaskList = mapper.Map<List<Data.TASKS>, List<Models.TaskModel>>(_context.GetTasks().ToList())
                };
                return View("TaskForm", viewModel);
            }
            if (taskModel.TId == 0)
            {
                taskModel.TId = _context.GetNextTaskId();
                taskModel.T_UId = _context.GetUsers().Single(u => u.UNAME == User.Identity.Name).UID;
                var task = mapper.Map<Models.TaskModel, Data.TASKS>(taskModel);
                _context.AddTask(task);
            }
            else
            {
                var taskInDb = _context.GetOneTask(taskModel.TId);
                mapper.Map(taskModel, taskInDb);
                _context.ModifyTasK(taskInDb);
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Details(int id)
        {
            var task = _context.GetOneTask(id);
            var taskModel = mapper.Map<Data.TASKS, Models.TaskModel>(task);
            Data.TASKS requiredTask = null;
            if (task.TREQUIREMENT != null)
                requiredTask = _context.GetOneTask(task.TREQUIREMENT.Value);

            var viewModel = new TaskDetailsViewModel
            {
                TaskModel = taskModel,
                RequiredTask = mapper.Map<Data.TASKS, Models.TaskModel>(requiredTask)
            };

            if (viewModel == null)
                return HttpNotFound();

            return View(viewModel);
        }
    }
}