﻿using AutoMapper;
using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagementLayer.Controllers
{
    public class UserController : Controller
    {
        IMapper mapper;
        ILogic _context;

        public UserController()
        {
            mapper = DTO.AutoMapperConfig.GetMapper();
            _context = new RealLogic();
        }
        // GET: User
        public ActionResult Details(int id)
        {
            var user = _context.GetOneUser(id);
            var userModel = mapper.Map<Data.USERS, Models.UserModel>(user);
          
            if (userModel == null)
                return HttpNotFound();

            return View(userModel);
        }
    }
}