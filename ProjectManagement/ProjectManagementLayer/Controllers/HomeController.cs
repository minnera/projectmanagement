﻿using AutoMapper;
using BusinessLogic;
using ProjectManagementLayer.Models;
using ProjectManagementLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagementLayer.Controllers
{
    public class HomeController : Controller
    {
        IMapper mapper;
        ILogic _context;

        public HomeController()
        {
            mapper = DTO.AutoMapperConfig.GetMapper();
            _context = new RealLogic();
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            var userId = _context.GetUsers().Single(u => u.UNAME == User.Identity.Name).UID;
            var tasks = _context.GetTasks().Where(t => t.T_UID == userId).Select(mapper.Map<Data.TASKS, Models.TaskModel>).ToList();
            var projects = _context.GetProjects().Select(mapper.Map<Data.PROJECTS, Models.ProjectModel>).ToList();
            var user = _context.GetUsers().Single(userInDb => userInDb.UNAME == User.Identity.Name);
            string userName = "";
            for (int i = 0; i < user.UNAME.Length; i++)
            {
                if (user.UNAME[i] == '@')
                {
                    break;
                }
                userName += user.UNAME[i];
            }
            userName = userName.Replace('.', ' ');
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            userName = textInfo.ToTitleCase(userName);

            var viewModel = new HomeIndexViewModel
            {
                Projects = projects,
                Tasks = tasks,
                User = mapper.Map<Data.USERS, Models.UserModel>(user),
                UserName = userName
            };
            return View(viewModel);
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult Login(UserModel userModel = null)
        {
            if(userModel != null)
            {
                if(_context.UserCheck(userModel.UName, userModel.UPassword.ToString())){
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Login");
            }

            return View(userModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(UserModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return View("Register", userModel);
            }
            if (userModel.UId == 0)
            {
                userModel.UId = _context.GetNextUserId();
                var user = mapper.Map<Models.UserModel, Data.USERS>(userModel);
                _context.AddUser(user);
            }
            else
            {
                var userInDb = _context.GetOneUser(userModel.UId);
                mapper.Map(userModel, userInDb);
                _context.ModifyUser(userInDb.UID, userInDb.UNAME, userInDb.UPASSWORD, userInDb.UGENDER, userInDb.UBDATE, userInDb.UFULLNAME);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}
