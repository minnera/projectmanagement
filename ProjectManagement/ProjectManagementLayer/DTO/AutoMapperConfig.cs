﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.DTO
{
    static class AutoMapperConfig
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.PROJECTS, DTO.ProjectDto>().ReverseMap();
                cfg.CreateMap<Data.GROUPS, DTO.GroupDto>().ReverseMap();
                cfg.CreateMap<Data.TASKS, DTO.TaskDto>().ReverseMap();
                cfg.CreateMap<Data.USERS, DTO.UserDto>().ReverseMap();
                cfg.CreateMap<Data.UGCONNT, DTO.UserGroupDto>().ReverseMap();
                cfg.CreateMap<Data.PROJECTS, Models.ProjectModel>().ReverseMap();
                cfg.CreateMap<Data.GROUPS, Models.GroupModel>().ReverseMap();
                cfg.CreateMap<Data.TASKS, Models.TaskModel>().ReverseMap();
                cfg.CreateMap<Data.USERS, Models.UserModel>().ReverseMap();
                cfg.CreateMap<Data.UPCONNT, Models.UserProjectModel>().ReverseMap();
                cfg.CreateMap<Data.UGCONNT, Models.UserGroupModel>().ReverseMap();
            });

            return config.CreateMapper();
        }
    }
}