﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.DTO
{
    public class TaskDto
    {
        [Required]
        public int TId { get; set; }

        [Required]
        public string TName { get; set; }

        [Required]
        public bool TIsDone { get; set; }

        [Required]
        public bool TIsArchived { get; set; }

        [Required]
        public string TComment { get; set; }

        public int? TRequirement { get; set; }

        public int T_PId { get; set; }
        public int T_UId { get; set; }
    }
}