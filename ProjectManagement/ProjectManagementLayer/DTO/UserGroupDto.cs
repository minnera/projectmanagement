﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.DTO
{
    public class UserGroupDto
    {

        [Required]
        public int UGId { get; set; }
        public int UId { get; set; }
        public int GId { get; set; }
        public string UGPost { get; set; }

        public List<DTO.GroupDto> GroupList { get; set; }
        public List<DTO.UserDto> UserList { get; set; }
    }
}