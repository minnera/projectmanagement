﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.DTO
{
    public class ProjectDto
    {
        [Required]
        public int PId { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 5)]
        public string PName { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 20)]
        public string PComment { get; set; }

        [Range(1, 10)]
        [Required]
        public int PPriority { get; set; }

        public bool PIsArchived { get; set; }

        [Required]
        public int P_GId_Maker { get; set; }

        [Required]
        public DateTime PDeadline { get; set; }
    }
}