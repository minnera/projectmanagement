﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.DTO
{
    public class GroupDto
    {
        [Required]
        public int GId { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 5)]
        public string GName { get; set; }

        [Required]
        public DateTime GDate { get; set; }

        [Required]
        public string GType { get; set; }

        [Required]
        public int GWage { get; set; }

        [Required]
        public DateTime GLastChange { get; set; }
    }
}