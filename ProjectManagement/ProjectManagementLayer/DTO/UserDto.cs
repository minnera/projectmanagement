﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagementLayer.DTO
{
    public class UserDto
    {
        [Required]
        public int UId { get; set; }

        [Required]
        public string UName { get; set; }

        [Required]
        public string UPassword { get; set; }

        [Required]
        public string UGender { get; set; }

        [Required]
        public DateTime UBdate { get; set; }

        [Required]
        public float UTotalSalary { get; set; }
    }
}