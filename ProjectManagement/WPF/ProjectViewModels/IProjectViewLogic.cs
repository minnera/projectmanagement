﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF.Models;

namespace WPF.ProjectViewModels
{
    interface IProjectViewLogic
    {
        void AddCmd();
        void ModifyCmd(ProjectVM p);
        void DelCmd(ProjectVM p);

        ObservableCollection<ProjectVM> GetProjectList();
    }
}
