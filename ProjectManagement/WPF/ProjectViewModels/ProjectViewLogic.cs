﻿using AutoMapper;
using BusinessLogic;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF.Models;
using WPF.Services;

namespace WPF.ProjectViewModels
{
    class ProjectViewLogic : IProjectViewLogic
    {

        ILogic BusiessLogic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }

        IProjectEditorService Editor
        {
            get { return ServiceLocator.Current.GetInstance<IProjectEditorService>(); }
        }

        IProjectViewModel VM
        {
            get { return ServiceLocator.Current.GetInstance<IProjectViewModel>(); }
        }

        IMapper Mapper
        {
            get { return ServiceLocator.Current.GetInstance<IMapper>(); }
        }



        public void AddCmd()
        {
            ProjectVM project = new ProjectVM();
            if (Editor.EditedProjet(project))
            {
                project.Pid = BusiessLogic.GetNextProjectId();
                Data.PROJECTS dbProject = Mapper.Map<ProjectVM, Data.PROJECTS>(project);
                BusiessLogic.AddProject(dbProject);
                VM.Projects.Add(project);
            }
        }

        public void DelCmd(ProjectVM p)
        {
            if (p == null)
            {
                Messenger.Default.Send(new NotificationMessage("NO PROJECT SELLECTED"));
                return;
            }
            BusiessLogic.DelProject(p.Pid);
            VM.Projects.Remove(p);
        }
        
        public void ModifyCmd(ProjectVM p)
        {
            if (p == null)
            {
                Messenger.Default.Send(new NotificationMessage("NO DEPT SELLECTED"));
                return;
            }
            ProjectVM clone = p.Clone();
            
            if (Editor.EditedProjet(clone))
            {  
               BusiessLogic.ModifyProject(p.Pid, clone.Pname, clone.Pisarchived, clone.Pispinned, clone.Pisdone, clone.Pisdeadline, clone.Ppriority, clone.Pcomment);
                p.CopyDataFrom(clone);
            }
        }

        public ObservableCollection<ProjectVM> GetProjectList()
        {
            var projectDB = BusiessLogic.GetProjects().ToList();
            return Mapper.Map<List<Data.PROJECTS>, ObservableCollection<ProjectVM>>(projectDB);
        }
    }
}
