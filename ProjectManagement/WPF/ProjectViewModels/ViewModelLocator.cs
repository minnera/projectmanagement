﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:WPF"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace WPF.ProjectViewModels
{
        public class ViewModelLocator
        {
            /// <summary>
            /// Initializes a new instance of the ViewModelLocator class.
            /// </summary>
            public ViewModelLocator()
            {
                ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

                ////if (ViewModelBase.IsInDesignModeStatic)
                ////{
                ////    // Create design time view services and models
                ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
                ////}
                ////else
                ////{
                ////    // Create run time view services and models
                ////    SimpleIoc.Default.Register<IDataService, DataService>();
                ////}

                //SimpleIoc.Default.Register<MainViewModel>();
            }

            public IProjectViewModel ProjectVM
            {
                get
                {
                    return ServiceLocator.Current.GetInstance<IProjectViewModel>();
                }
            }

            public IProjectEditorViewModel EditorVM
            {
                get
                {
                    return ServiceLocator.Current.GetInstance<IProjectEditorViewModel>();
                }
            }

            public static void Cleanup()
            {
                // TODO Clear the ViewModels
            }
        }
    }
