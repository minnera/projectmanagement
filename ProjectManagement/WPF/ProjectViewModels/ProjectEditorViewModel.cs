﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WPF.Models;

namespace WPF.ProjectViewModels
{
    public class ProjectEditorViewModel : ViewModelBase, IProjectEditorViewModel
    {
        ProjectVM editedProject;
        public ProjectVM EditedProject
        {
            get { return editedProject; }
            set { Set(ref editedProject, value); }
        }

        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public ProjectEditorViewModel()
        {
            OkCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send(true);
                }
                );
            CancelCommand = new RelayCommand(() => Messenger.Default.Send(false));
        }
    }
}
