﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WPF.Models;
using Microsoft.Practices.ServiceLocation;
using GalaSoft.MvvmLight.Command;

namespace WPF.ProjectViewModels
{
    public class ProjectViewModel : ViewModelBase, IProjectViewModel
    {
        ProjectVM selectedProject;
        ObservableCollection<ProjectVM> projects;
        
        public ProjectVM SelectedProject
        {
            get { return selectedProject; }
            set { Set(ref selectedProject, value); }
        }
        public ObservableCollection<ProjectVM> Projects {
            get { return projects; }
            set { Set(ref projects, value); } }

        public ICommand AddCommand { get; private set; }

        public ICommand ModifyCommand { get; private set; }

        public ICommand DelCommand { get; private set; }

        IProjectViewLogic ViewLogic
        {
            get { return ServiceLocator.Current.GetInstance<IProjectViewLogic>(); }
        }

        public ProjectViewModel()
        {
            AddCommand = new RelayCommand(ViewLogic.AddCmd);
            ModifyCommand = new RelayCommand<ProjectVM>(ViewLogic.ModifyCmd);
            DelCommand = new RelayCommand<ProjectVM>(ViewLogic.DelCmd);

            projects = ViewLogic.GetProjectList();
        }
    }
}
