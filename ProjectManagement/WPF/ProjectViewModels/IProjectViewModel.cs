﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WPF.Models;

namespace WPF.ProjectViewModels
{
    public interface IProjectViewModel
    {
        ProjectVM SelectedProject { get; set; }
        ObservableCollection<ProjectVM> Projects { get; set; }

        ICommand AddCommand { get; }
        ICommand ModifyCommand { get; }
        ICommand DelCommand { get; }
    }
}
