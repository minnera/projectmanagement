﻿using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF.Models;
using WPF.ProjectViewModels;

namespace WPF.ProjectWindows.Windows
{
    /// <summary>
    /// Interaction logic for ProjectEditorWindow.xaml
    /// </summary>
    public partial class ProjectEditorWindow 
    {
        public ProjectEditorWindow(ProjectVM p = null)
        {
            InitializeComponent();

            ServiceLocator.Current.GetInstance<IProjectEditorViewModel>().EditedProject = p;

        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Register<bool>(this, ProcessResult);
        }

        private void ProcessResult(bool msg)
        {
            DialogResult = msg;
            Close();
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }
    }
}
