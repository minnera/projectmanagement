﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF.Models;

namespace WPF.Services
{
    interface IProjectEditorService
    {
        bool EditedProjet(ProjectVM p);
    }
}
