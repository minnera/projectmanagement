﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF.Models;
using WPF.ProjectWindows.Windows;

namespace WPF.Services
{
    class ProjectEditorService : IProjectEditorService
    {
        public bool EditedProjet(ProjectVM p)
        {
            ProjectEditorWindow win = new ProjectEditorWindow(p);
            return win.ShowDialog().Value;
        }
    }
}
