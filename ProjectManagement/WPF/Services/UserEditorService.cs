﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF.Models;
using WPF.UserWindows.Windows;

namespace WPF.Services
{
    class UserEditorService : IUserEditorService
    {
        public bool EditedUser(UserVM u)
        {
            UserEditorWindow win = new UserEditorWindow(u);
            return win.ShowDialog().Value;
        }
    }
}
