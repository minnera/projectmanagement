﻿using AutoMapper;
using BusinessLogic;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WPF.Models;
using WPF.ProjectViewModels;
using WPF.Services;
using WPF.UserViewModels;

namespace WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        //konstruktor ioc -> serviceLocator
        //   
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            SimpleIoc.Default.Register<IUserViewModel, UserViewModel>();
            SimpleIoc.Default.Register<IUserViewLogic, UserViewLogic>();
            SimpleIoc.Default.Register<IUserEditorViewModel, UserEditorViewModel>();
            SimpleIoc.Default.Register<IUserEditorService, UserEditorService>();

            SimpleIoc.Default.Register<IProjectViewModel, ProjectViewModel>();
            SimpleIoc.Default.Register<IProjectViewLogic, ProjectViewLogic>();
            SimpleIoc.Default.Register<IProjectEditorViewModel, ProjectEditorViewModel>();
            SimpleIoc.Default.Register<IProjectEditorService, ProjectEditorService>();

            SimpleIoc.Default.Register<ILogic>(() => new FakeLogic());
            SimpleIoc.Default.Register<IMapper>(() => AutoMapperConfig.GetMapper());
        }
    }
}
