﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WPF.Models;

namespace WPF.UserViewModels
{
    public interface IUserEditorViewModel
    {
        UserVM EditedUser { get; set; }

        ICommand OkCommand { get; }
        ICommand CancelCommand { get; }
    }
}
