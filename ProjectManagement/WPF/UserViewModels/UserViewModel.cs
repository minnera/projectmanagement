﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WPF.Models;

namespace WPF.UserViewModels
{
    class UserViewModel : ViewModelBase, IUserViewModel
    {
        UserVM selectedUser;
        ObservableCollection<UserVM> users;

        public UserVM SelectedUser { get => selectedUser;
            set { Set(ref selectedUser, value); } }
        public ObservableCollection<UserVM> Users { get => users;
            set { Set(ref users, value); } }

        public ICommand AddCommand { get; private set; }

        public ICommand ModifyCommand { get; private set; }

        public ICommand DeleteCommand { get; private set; }

        public ICommand XMLSerializer { get; private set; }

        public ICommand JSON { get; private set; }

        IUserViewLogic ViewLogic
        {
            get { return ServiceLocator.Current.GetInstance<IUserViewLogic>(); }
        }

       

        public UserViewModel()
        {
            AddCommand = new RelayCommand(ViewLogic.AddCmd);
            ModifyCommand = new RelayCommand<UserVM>(ViewLogic.ModifyCmd);
            DeleteCommand = new RelayCommand<UserVM>(ViewLogic.DelCmd);
            XMLSerializer = new RelayCommand(ViewLogic.XML);

            users = ViewLogic.GetUserList();
        }
    }
}
