﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WPF.Models;

namespace WPF.UserViewModels
{
    public class UserEditorViewModel : ViewModelBase, IUserEditorViewModel
    {

        UserVM editedUser;
        public UserVM EditedUser
        {
            get { return editedUser; }
            set { Set(ref editedUser, value); }
        }

        public ICommand OkCommand { get; private set; }

        public ICommand CancelCommand { get; private set; }

        public UserEditorViewModel()
        {
            OkCommand = new RelayCommand(() => Messenger.Default.Send(true));
            CancelCommand = new RelayCommand(() => Messenger.Default.Send(false));
        }
    }
}
