﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF.Models;

namespace WPF.UserViewModels
{
    interface IUserViewLogic
    {
        void AddCmd();
        void ModifyCmd(UserVM u);
        void DelCmd(UserVM u);
        void XML();
        void JSON();

        ObservableCollection<UserVM> GetUserList();
    }
}
