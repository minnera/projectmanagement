﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WPF.Models;


namespace WPF.UserViewModels
{
    public interface IUserViewModel
    {
        UserVM SelectedUser { get; set; }
        ObservableCollection<UserVM> Users { get; set; }

        ICommand AddCommand { get; }
        ICommand ModifyCommand { get; }
        ICommand DeleteCommand { get; }
        ICommand XMLSerializer { get; } 
        ICommand JSON { get; }
    }
}
