﻿using AutoMapper;
using BusinessLogic;
using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;
using Microsoft.Practices.ServiceLocation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;
using WPF.Models;
using WPF.Services;

namespace WPF.UserViewModels
{
    
    class UserViewLogic : IUserViewLogic    
    {
        private const string FILENAME = "users.xml";

        ILogic BusinessLogic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }

        IUserEditorService Editor
        {
            get { return ServiceLocator.Current.GetInstance<IUserEditorService>(); }
        }

        IUserViewModel VM
        {
            get { return ServiceLocator.Current.GetInstance<IUserViewModel>(); }
        }

        IMapper Mapper
        {
            get { return ServiceLocator.Current.GetInstance<IMapper>(); }
        }

        public void AddCmd()
        {
            UserVM user = new UserVM();
            if (Editor.EditedUser(user))
            {
                user.Uid = BusinessLogic.GetNextUserId();
                Data.USERS dbUser = Mapper.Map<UserVM, Data.USERS>(user);
                BusinessLogic.AddUser(dbUser);
                VM.Users.Add(user);
            }
            
        }

        public void DelCmd(UserVM u)
        {
            if (u == null)
            {
                Messenger.Default.Send(new NotificationMessage("NO USER SELLECTED")); 
            }
            BusinessLogic.DelUser(u.Uid);
            VM.Users.Remove(u);
        }

        public void ModifyCmd(UserVM u)
        {
            if (u == null)
            {
                Messenger.Default.Send(new NotificationMessage("NO USER SELLECTED"));
                return;
            }
            UserVM clone = u.Clone();
            if (Editor.EditedUser(clone))
            {               //a clone nem biztos, h elfogadja a DateTime valtozot a bdate ben
                BusinessLogic.ModifyUser(u.Uid, clone.Uname, clone.Upassworld, clone.Ugender, clone.Ubdate, clone.UfullName);
                u.CopyDataFrom(clone);
            }
        }
        
        public void XML()
        {


            var users = BusinessLogic.GetUsers().ToList();
            var converted = Mapper.Map<List<Data.USERS>, ObservableCollection<UserVM>>(users);


            XmlSerializer xms = new XmlSerializer(typeof(ObservableCollection<UserVM>));
            using (FileStream fs = new FileStream(FILENAME, FileMode.Create))
            {
                xms.Serialize(fs, converted);
            }
            Messenger.Default.Send(new NotificationMessage("DONE!"));

        }
        
        public void JSON()
        {
            Messenger.Default.Send(new NotificationMessage("Dowlandig..."));
            string url = "";
            string json = new WebClient().DownloadString(url);
            Messenger.Default.Send(new NotificationMessage(json));
            var list = JsonConvert.DeserializeObject<List<UserDTO>>(json);
            foreach (var item in list)
            {
                if (VM.Users.Any(x => x.Uid == item.UID))
                {
                    BusinessLogic.ModifyUser(item.UID, item.UNAME, item.UPASSWORLD, item.UGENDER, item.UBDATE, item.UFULLNAME);
                    Messenger.Default.Send(new NotificationMessage("UPD: " + item));
                }
                else
                {
                    Data.USERS dbUsers = Mapper.Map<UserDTO, Data.USERS>(item);
                    BusinessLogic.AddUser(dbUsers);
                    Messenger.Default.Send(new NotificationMessage("INS: " + item.UID));
                }
            }
            VM.Users = GetUserList();
        }

        public ObservableCollection<UserVM> GetUserList()
        {
            var usersDB = BusinessLogic.GetUsers().ToList();
            return Mapper.Map<List<Data.USERS>, ObservableCollection<UserVM>>(usersDB);
        }

       
    }
}
