﻿using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF.Models;
using WPF.UserViewModels;

namespace WPF.UserWindows.Windows
{
    /// <summary>
    /// Interaction logic for UserEditorWindow.xaml
    /// </summary>
    public partial class UserEditorWindow 
    {
        public UserEditorWindow(UserVM u = null)
        {
            InitializeComponent();

            ServiceLocator.Current.GetInstance<IUserEditorViewModel>().EditedUser = u;
            //ServiceLocator.Current.GetInstance<IUserEditorViewModel>
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Register<bool>(this, ProcessResult);
        }

        private void ProcessResult(bool msg)
        {
            DialogResult = msg;
            Close();
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }
    }
}
