﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF.ProjectWindows;
using WPF.UserWindows;

namespace WPF.UserWindows
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView
    {
        public MainView()
        {
            InitializeComponent();
        }

        
        private void User_Editor(object sender, RoutedEventArgs e)
        {
            UserView win = new UserView();
            win.ShowDialog();
        }

        private void Project_Editor(object sender, RoutedEventArgs e)
        {
            ProjectView win = new ProjectView();
            win.ShowDialog();
        }
    }
}
