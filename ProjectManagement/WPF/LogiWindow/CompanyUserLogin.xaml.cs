﻿using BusinessLogic;
using GalaSoft.MvvmLight;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF.Models;
using WPF.UserWindows;

namespace WPF.LogiWindow
{
    /// <summary>
    /// Interaction logic for CompanyUserLogin.xaml
    /// </summary>
    public partial class CompanyUserLogin 
    {
        ILogic BusinessLogic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }
        
        public CompanyUserLogin()
        {
            InitializeComponent();
        }

        private void Login(object sender, RoutedEventArgs e)
        {
            
            if (textBoxUser.Text.Length == 0)
            {
                MessageBox.Show("Enter an User");
            }
            else
            {
                string user = textBoxUser.Text;
                string pass = passBox.Password;
                bool result = BusinessLogic.CompanyCheck(user, pass);
                if (result)
                {
                    MainView win = new MainView();
                    win.ShowDialog();
                }
                
            }
            
        }
    }
}
