﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF.Models
{
    public class UserVM : ObservableObject
    {
        int UID;
        string UNAME;
        string UPASSWORLD;
        string UGENDER;
        DateTime UBDATE;
        string UFULLNAME;
        int UTOTALSALARY;

        public int Uid { get => UID;
            set
            {
                Set(ref UID, value);
            }
        }
        public string Uname { get => UNAME;
            set
            {
                Set(ref UNAME, value);
            }
        }
        public string Upassworld { get => UPASSWORLD;
            set
            {
                Set(ref UPASSWORLD, value);
            }
        }
        public string Ugender { get => UGENDER;
            set
            {
                Set(ref UGENDER, value);
            }
        }
        public DateTime Ubdate { get => UBDATE;
            set
            {
                Set(ref UBDATE, value);
            }
        }
        public string UfullName { get => UFULLNAME;
            set
            {
                Set(ref UFULLNAME, value);
            }
        }
        public int UtotalSalary { get => UTOTALSALARY;
            set
            {
                Set(ref UTOTALSALARY, value);
            }
        }

        public UserVM Clone()
        {
            return MemberwiseClone() as UserVM;
        }

        
        public void CopyDataFrom(UserVM other)
        {
            this.Uid = other.Uid;
            this.Uname = other.Uname;
            this.Upassworld = other.Upassworld;
            this.Ugender = other.Ugender;
            this.Ubdate = other.Ubdate;
            this.UfullName = UfullName;
            this.UtotalSalary = other.UtotalSalary;
        }
    }
}
