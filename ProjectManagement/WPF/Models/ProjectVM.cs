﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF.Models
{
    public class ProjectVM : ObservableObject
    {
        int PID;
        string PNAME;
        string PIDCUSTUMER;
        string PCIDMAKER;
        bool PISARCHIVED;
        bool PISPINNED;
        bool PISDONE;
        DateTime PISDEADLINE;
        int PPRIORITY;
        DateTime PRECORDED;
        int PPROFIT;
        string PCOMMENT;
        DateTime PLASTMODIFY;

        public int Pid { get => PID;
            set
            {
                Set(ref PID, value);
            }
        }
        public string Pname { get => PNAME;
            set
            {
                Set(ref PNAME, value);
            }
        }
        public string Pcidcustomer { get => PIDCUSTUMER;
            set {
                Set(ref PIDCUSTUMER, value);
            }
        }
        public string Pgidmaker { get => PCIDMAKER;
            set
            {
                Set(ref PCIDMAKER, value);
            }
        }
        public bool Pisarchived { get => PISARCHIVED;
            set
            {
                Set(ref PISARCHIVED, value);
            }
        }
        public bool Pispinned { get => PISPINNED;
            set
            {
                Set(ref PISPINNED, value);
            }
        }
        public bool Pisdone { get => PISDONE;
            set
            {
                Set(ref PISDONE, value);
            }
        }
        public DateTime Pisdeadline { get => PISDEADLINE;
            set
            {
                Set(ref PISDEADLINE, value);
            }
        }
        public int Ppriority { get => PPRIORITY;
            set
            {
                Set(ref PPRIORITY, value);
            }
        }
        public DateTime Precorded { get => PRECORDED;
            set
            {
                Set(ref PRECORDED, value);
            }
        }
        public int Pprofit { get => PPROFIT;
            set
            {
                Set(ref PPROFIT, value);
            }
        }
        public string Pcomment { get => PCOMMENT;
            set
            {
                Set(ref PCOMMENT, value);
            }
        }
        public DateTime Plastmodify { get => PLASTMODIFY;
            set
            {
                Set(ref PLASTMODIFY, value);
            }
        }


        public ProjectVM Clone()
        {
            return MemberwiseClone() as ProjectVM;
        }
        
        public void CopyDataFrom(ProjectVM other)
        {
            this.Pid = other.Pid;
            this.Pname = other.Pname;
            this.Pcidcustomer = other.Pcidcustomer;
            this.Pgidmaker = other.Pgidmaker;
            this.PISARCHIVED = other.PISARCHIVED;
            this.Pispinned = other.Pispinned;
            this.Pisdone = other.Pisdone;
            this.Pisdeadline = other.Pisdeadline;
            this.Ppriority = other.Ppriority;
            this.Precorded = other.Precorded;
            this.Pprofit = other.Pprofit;
            this.Pcomment = other.Pcomment;
            this.Plastmodify = other.Plastmodify;
        }
    }
}
