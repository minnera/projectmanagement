﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF.Models
{
    public class UserDTO
    {
        public int UID { get; set; }
        public string UNAME { get; set; }
        public string UPASSWORLD { get; set; }
        public string UGENDER { get; set; }
        public DateTime UBDATE { get; set; }
        public string UFULLNAME { get; set; }
        public int UTOTALSALARY { get; set; }
    }
}
