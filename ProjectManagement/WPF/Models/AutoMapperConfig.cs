﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF.Models
{
    class AutoMapperConfig
    {
        public static IMapper GetMapper()
        { 
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.USERS, UserVM>().ReverseMap();
                cfg.CreateMap<Data.PROJECTS, ProjectVM>().ReverseMap();

            });
            return config.CreateMapper();
        }
    }
}
