﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;
using BusinessLogic.BL_Objects;
using Data;

namespace DB
{
    public class Class1
    {
        static void Main(string[] args)
        {
            ILogic logic = new FakeLogic();
            Console.WriteLine("\nElső teszt:\n");
            IQueryable<UsersWithCompaniesQueryResult> q = logic.GetUsersWithCompanies();
            foreach (UsersWithCompaniesQueryResult user in q)
            {
                Console.WriteLine(user.Username + " : " + user.Companies);
            }

            Console.WriteLine("\nCsoportok felhasználónevekkel teszt:\n");
            IQueryable<GroupsWithUsersQueryResult> w = logic.GetGroupsWithUsers();
            foreach(GroupsWithUsersQueryResult group in w)
            {
                Console.WriteLine(group.Groupname + " : " + group.Usernames);
            }


            //users: 1-tony, 2-kate
            //companies: 1-telekom, 2-otp
            //projects: 1-macerás, 2-kocsi, 3-macska, 4-sim(done)
            //UP: 1-2, 1-3, 2-1, 2-3, 2-4


            Console.WriteLine("\nMásodik teszt:\n");
            int userid = 2;
            foreach (PROJECTS p in logic.GetUsersProjectsUnderProcess(userid))
            {
                Console.WriteLine(p.PNAME + " : " + logic.GetOneUser(userid).UFULLNAME);
            }

            Console.WriteLine("\nHarmadik teszt:\n");
            foreach (UCCONNT connection in logic.GetUserCompanyConnections())
            {
                Console.WriteLine(connection.UID + " : " + connection.UCJOB);
            }
            Console.WriteLine();
            logic.AddUserForCompany(2, 2, "pénztáros");
            foreach (UCCONNT connection in logic.GetUserCompanyConnections())
            {
                Console.WriteLine(connection.UID + " : " + connection.UCJOB);
            }

            Console.WriteLine("\nNegyedik teszt - Macskás Projekt törlése:\n");
            foreach (PROJECTS project in logic.GetProjects())
            {
                Console.WriteLine(project.PNAME);
            }
            foreach (UPCONNT connection in logic.GetUserProjectsConnections())
            {
                Console.WriteLine(connection.PID);
            }
            foreach (TASKS task in logic.GetTasks())
            {
                Console.WriteLine(task.TNAME + " , " + task.T_PID);
            }
            logic.DelProject(3);
            Console.WriteLine();
            foreach (PROJECTS project in logic.GetProjects())
            {
                Console.WriteLine(project.PNAME);
            }
            foreach (UPCONNT connection in logic.GetUserProjectsConnections())
            {
                Console.WriteLine(connection.PID);
            }
            foreach (TASKS task in logic.GetTasks())
            {
                Console.WriteLine(task.TNAME + " , " + task.T_PID);
            }

            Console.WriteLine("\njelszó teszt\n");

            string jelszo = "mattmonitorkepernyo";

            Console.WriteLine(logic.UserCheck("kate", jelszo));

            Console.ReadKey();

            //ide még kéne

            //------------------------------------RealLogic Tests

            RealLogic reallogic = new RealLogic();

            Console.WriteLine("\nNulladik teszt, az adatbázis feltöltése\nEz itt lassú lesz.\n");

            //ha nincs elem így nincs max index, akkor milyen indexet kap az első elem getnextindex-szel?
            reallogic.AddCompany("Telekom", "ezleszajelszo", new DateTime(2010, 01, 02), "Magyar Telekom", "kommunikáció", 123456789);
            reallogic.AddCompanyGroup(1, "Panaszkezelés", "private", "business", 120000);
            reallogic.AddUser("Bela", "feketebillentyuzet", "male", new DateTime(1992, 11, 10), "Kovács Béla");
            reallogic.AddProjectToGroup(1, "Elveszett sim", 1, new DateTime(2017, 06, 20), 5, "Nagyon sürgős");
            reallogic.AddUserForCompany(1, 1, "beosztott");
            reallogic.AddUserProjectConnection(new UPCONNT() { PID = 1, UID = 1, UPID = 1 });

            reallogic.AddCompany("OTP", "ezleszajelszo2", new DateTime(2012, 02, 02), "Magyar OTP BANK", "bankolás", 987654321);
            reallogic.AddCompanyGroup(1, "Pénztárosok", "private", "business", 100000);
            reallogic.AddUser("Kata", "ezauserpassword2", "female", new DateTime(1993, 11, 11), "Nagy Kata");
            reallogic.AddProjectToGroup(2, "Pénzváltás", 2, new DateTime(2017, 06, 30), 1, "Nem sürgős");
            reallogic.AddUserForCompany(2, 2, "pénztáros");
            reallogic.AddUserProjectConnection(new UPCONNT() { PID = 2, UID = 2, UPID = 2 });

            //kész project béla neve alatt

            reallogic.AddProjectToGroup(1, "Befejezett telekom", 1, new DateTime(2017, 06, 01), 2, "Megcsináltuk");
            reallogic.ModifyProject(3, "Befejezett Telekom", false, false, true, new DateTime(2017, 06, 01), 2, "Megcsináltuk");
            reallogic.AddUserProjectConnection(new UPCONNT() { PID = 3, UID = 1, UPID = 3 });

            reallogic.AddUserForCompany(2, 1, "panaszkezelő");//kata telekomban

            userid = 1;

            //----------------------------------------------------------------------------

            Console.WriteLine("\nElső RealLogic Teszt:\n");
            foreach (PROJECTS p in reallogic.GetUsersProjectsUnderProcess(userid))
            {
                Console.WriteLine(p.PNAME + " : " + reallogic.GetOneUser(userid).UFULLNAME);
            }

            Console.WriteLine("\njelszó teszt\n");

            jelszo = "feketebillentyuzet";

            Console.WriteLine(reallogic.UserCheck("Bela", jelszo));
            
            Console.ReadKey();
        }
    }
}