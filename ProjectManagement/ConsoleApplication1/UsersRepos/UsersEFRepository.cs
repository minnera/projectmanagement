﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using Data;
using Repository.GenericRepos;

namespace Repository.UsersRepos
{
    public class UsersEFRepository : EFRepository<USERS>, IUsersRepository
    {
        public UsersEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override USERS GetById(int id)
        {
            return Get(akt => akt.UID == id).SingleOrDefault();
        }

        public void Modify(int id, string name, string password, string gender, DateTime? bdate, string fullname)
        {
            USERS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            //mikor beírja a user a neve, ott le kell kezelni, hogy csak angol abc betűje lehet vagy szám
            if (name != null)
            {
                akt.UNAME = name;
            }
            //TODO password titkosítás szépítése
            if (password != null)
            {
                //összefűzzök a jelszót, valami random szöveget és a usernevet
                string salt = "projectmanagement";
                string output = password + salt + name;
                SHA256 sha = SHA256Managed.Create();
                byte[] hashValue = System.Text.Encoding.UTF8.GetBytes(output);
                hashValue = sha.ComputeHash(hashValue);
                output = String.Join(String.Empty, Array.ConvertAll(hashValue, x => x.ToString("X2")));
                akt.UPASSWORD = output;
                //Console.WriteLine(output);
                //https://stackoverflow.com/a/311382/3079988
            }
            //beírásnál x-elni lehetne szerintem a gendert és az alapján biztos, hogy csak a megadott lehetőségek jönnének stringben
            if (gender != null)
            {
                akt.UGENDER = gender;
            }
            akt.UBDATE = bdate;
            if (fullname != null)
            {
                akt.UFULLNAME = fullname;
            }
            context.SaveChanges();
        }

        public void ModifyTotalSalary(int id, double totalsalary)
        {
            USERS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            akt.UTOTALSALARY = totalsalary;
            context.SaveChanges();
        }
    }
}