﻿using System;
using System.Linq;
using System.Security.Cryptography;
using Data;
using Repository.GenericRepos;

namespace Repository.UsersRepos
{
    public class UsersListRepository : ListRepository<USERS>, IUsersRepository
    {
        public UsersListRepository()
        {
            Insert(new USERS()
            {
                UID = 1,
                UNAME = "tony",
                UPASSWORD = "A3AD583055C93A1B32FD0E24FF6249DF8F358B02FBF5FFFF9C7B6E57C5988BE4", // "feketebillentyuzet"
                UGENDER = "male",
                UBDATE = new DateTime(1990, 10, 04),
                UFULLNAME = "Tony Stark",
                UTOTALSALARY = 0
            });

            Insert(new USERS()
            {
                UID = 2,
                UNAME = "kate",
                UPASSWORD = "8A107A8C7112D24B926A9B192E989FAD14DA762320C4FAA37B485A6F0311643D", // "mattmonitorkepernyo"
                UGENDER = "female",
                UBDATE = new DateTime(1990, 12, 12),
                UFULLNAME = "Kate Stark",
                UTOTALSALARY = 0
            });

            Insert(new USERS()
            {
                UID = 3,
                UNAME = "tamara",
                UPASSWORD = "A3AD583055C93A1B32FD0E24FF6249DF8F358B02FBF5FFFF9C7B6E57C5988BE4", // "feketebillentyuzet"
                UGENDER = "female",
                UBDATE = new DateTime(1991, 02, 24),
                UFULLNAME = "Tamara Smith",
                UTOTALSALARY = 0
            });

            Insert(new USERS()
            {
                UID = 4,
                UNAME = "jack",
                UPASSWORD = "A3AD583055C93A1B32FD0E24FF6249DF8F358B02FBF5FFFF9C7B6E57C5988BE4", // "feketebillentyuzet"
                UGENDER = "male",
                UBDATE = new DateTime(1988, 08, 18),
                UFULLNAME = "Jack Sparrow",
                UTOTALSALARY = 0
            });

            Insert(new USERS()
            {
                UID = 5,
                UNAME = "johny",
                UPASSWORD = "A3AD583055C93A1B32FD0E24FF6249DF8F358B02FBF5FFFF9C7B6E57C5988BE4", // "feketebillentyuzet"
                UGENDER = "male",
                UBDATE = new DateTime(1999, 11, 30),
                UFULLNAME = "Johny Potter",
                UTOTALSALARY = 0
            });

            Insert(new USERS()
            {
                UID = 6,
                UNAME = "lily",
                UPASSWORD = "A3AD583055C93A1B32FD0E24FF6249DF8F358B02FBF5FFFF9C7B6E57C5988BE4", // "feketebillentyuzet"
                UGENDER = "female",
                UBDATE = new DateTime(1989, 04, 28),
                UFULLNAME = "Lily Potter",
                UTOTALSALARY = 0
            });

            Insert(new USERS()
            {
                UID = 7,
                UNAME = "molly",
                UPASSWORD = "A3AD583055C93A1B32FD0E24FF6249DF8F358B02FBF5FFFF9C7B6E57C5988BE4", // "feketebillentyuzet"
                UGENDER = "female",
                UBDATE = new DateTime(1972, 05, 11),
                UFULLNAME = "Molly Sparkle",
                UTOTALSALARY = 0
            });

            Insert(new USERS()
            {
                UID = 8,
                UNAME = "edward",
                UPASSWORD = "A3AD583055C93A1B32FD0E24FF6249DF8F358B02FBF5FFFF9C7B6E57C5988BE4", // "feketebillentyuzet"
                UGENDER = "male",
                UBDATE = new DateTime(1993, 09, 10),
                UFULLNAME = "Edward ",
                UTOTALSALARY = 0
            });

            Insert(new USERS()
            {
                UID = 9,
                UNAME = "hank",
                UPASSWORD = "A3AD583055C93A1B32FD0E24FF6249DF8F358B02FBF5FFFF9C7B6E57C5988BE4", // "feketebillentyuzet"
                UGENDER = "male",
                UBDATE = new DateTime(1990, 12, 04),
                UFULLNAME = "Hank Thomas",
                UTOTALSALARY = 0
            });
        }

        public override USERS GetById(int id)
        {
            return Get(akt => akt.UID == id).SingleOrDefault();
        }

        public void Modify(int id, string name, string password, string gender, DateTime? bdate, string fullname)
        {
            USERS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            //mikor beírja a user a neve, ott le kell kezelni, hogy csak angol abc betűje lehet vagy szám
            if (name != null)
            {
                akt.UNAME = name;
            }
            //TODO password titkosítás szépítése
            if (password != null)
            {
                //összefűzzök a jelszót, valami random szöveget és a usernevet
                string salt = "projectmanagement";
                string output = password + salt + name;
                SHA256 sha = SHA256Managed.Create();
                byte[] hashValue = System.Text.Encoding.UTF8.GetBytes(output);
                hashValue = sha.ComputeHash(hashValue);
                output = String.Join(String.Empty, Array.ConvertAll(hashValue, x => x.ToString("X2")));
                akt.UPASSWORD = output;
                //https://stackoverflow.com/a/311382/3079988
            }
            //beírásnál x-elni lehetne szerintem a gendert és az alapján biztos, hogy csak a megadott lehetőségek jönnének stringben
            if (gender != null)
            {
                akt.UGENDER = gender;
            }
            akt.UBDATE = bdate;
            if (fullname != null)
            {
                akt.UFULLNAME = fullname;
            }
        }

        public void ModifyTotalSalary(int id, double totalsalary)
        {
            USERS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            akt.UTOTALSALARY = totalsalary;
        }
    }
}