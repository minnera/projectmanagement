﻿using System;
using Data;
using Repository.GenericRepos;

namespace Repository.UsersRepos
{
    public interface IUsersRepository : IRepository<USERS>
    {
        //a user saját maga tudja ezeket változtatni
        void Modify(int id, string name, string password, string gender, DateTime? bdate, string fullname);
        //kifizetéseknél változik
        void ModifyTotalSalary(int id, double totalsalary);
    }
}