﻿using System;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.UGConnTRepos
{
    public class UGConnTListRepository : ListRepository<UGCONNT>, IUGConnTRepository
    {
        public UGConnTListRepository()
        {
            Insert(new UGCONNT()
            {
                UGID = 1,
                UID = 1,
                GID = 2,
                UGPOST = "owner"
            });

            Insert(new UGCONNT()
            {
                UGID = 2,
                UID = 2,
                GID = 1,
                UGPOST = "user"
            });

            Insert(new UGCONNT()
            {
                UGID = 3,
                UID = 2,
                GID = 2,
                UGPOST = "mod"
            });
        }

        public override UGCONNT GetById(int id)
        {
            return Get(akt => akt.UGID == id).SingleOrDefault();
        }

        public void Modify(int id, string post)
        {
            UGCONNT akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            //fentebb le kell kezelni, hogy a lehetséges post fajtákra lehessen csak módosítani TODO
            if (post != null)
            {
                akt.UGPOST = post;
            }
        }
    }
}