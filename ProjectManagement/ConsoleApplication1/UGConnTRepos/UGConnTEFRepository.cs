﻿using System;
using System.Data.Entity;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.UGConnTRepos
{
    public class UGConnTEFRepository : EFRepository<UGCONNT>, IUGConnTRepository
    {
        public UGConnTEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override UGCONNT GetById(int id)
        {
            return Get(akt => akt.UGID == id).SingleOrDefault();
        }

        public void Modify(int id, string post)
        {
            //kell egy másolat példány
            UGCONNT akt = GetById(id);
            //lekezeljük, ha nem létezik a megadott id-jű elem
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            //módosítjuk az új adatokra, ha azok nem null-ok
            //fentebb le kell kezelni, hogy a lehetséges post fajtákra lehessen csak módosítani TODO
            if (post != null)
            {
                akt.UGPOST = post;
            }
            //elmentjük a változásokat
            context.SaveChanges();
        }

    }
}