﻿using Data;
using Repository.GenericRepos;

namespace Repository.UGConnTRepos
{
    public interface IUGConnTRepository : IRepository<UGCONNT>
    {
        void Modify(int id, string post);
    }
}