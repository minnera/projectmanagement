﻿using System;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.GroupsRepos
{
    public class GroupsListRepository : ListRepository<GROUPS>, IGroupsRepository
    {
        public GroupsListRepository()
        {
            Insert(new GROUPS()
            {
                GID = 1,
                GNAME = "Telekom Panaszkezelés",
                GVISIBILITY = "company",
                GDATE = DateTime.Now,
                GTYPE = "business",
                GWAGE = 90000,
                GLASTCHANGE = DateTime.Now
            });

            Insert(new GROUPS()
            {
                GID = 2,
                GNAME = "Saját TODO lista",
                GVISIBILITY = "private",
                GDATE = DateTime.Now,
                GTYPE = "private",
                GWAGE = 0,
                GLASTCHANGE = DateTime.Now
            });
        }

        public override GROUPS GetById(int id)
        {
            return Get(akt => akt.GID == id).SingleOrDefault();
        }

        public void Modify(int id, string name, string type, double wage)
        {
            GROUPS akt = GetById(id);

            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            if (name != null)
            {
                akt.GNAME = name;
            }
            if (type != null)
            {
                akt.GTYPE = type;
            }
            akt.GWAGE = wage;
        }

        public void UpdateLastChangeDate(int id, DateTime lastchange)
        {
            GROUPS akt = GetById(id);

            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            akt.GLASTCHANGE = lastchange;
        }
    }
}