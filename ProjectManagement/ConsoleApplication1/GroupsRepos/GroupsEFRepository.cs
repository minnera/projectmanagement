﻿using System;
using System.Data.Entity;
using System.Linq;
using Repository.GenericRepos;
using Data;

namespace Repository.GroupsRepos
{
    public class GroupsEFRepository : EFRepository<GROUPS>, IGroupsRepository
    {
        public GroupsEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override GROUPS GetById(int id)
        {
            return Get(akt => akt.GID == id).SingleOrDefault();
        }

        public void Modify(int id, string name, string type, double wage)
        {
            GROUPS akt = GetById(id);

            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            if (name != null)
            {
                akt.GNAME = name;
            }
            if (type != null)
            {
                akt.GTYPE = type;
            }
            akt.GWAGE = wage;
            context.SaveChanges();
        }

        public void UpdateLastChangeDate(int id, DateTime lastchange)
        {
            GROUPS akt = GetById(id);

            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            akt.GLASTCHANGE = lastchange;
            context.SaveChanges();
        }
    }
}