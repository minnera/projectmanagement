﻿using System;
using Data;
using Repository.GenericRepos;

namespace Repository.GroupsRepos
{
    public interface IGroupsRepository : IRepository<GROUPS>
    {
        //név, láthatóság, típus és bér változtatható
        void Modify(int id, string name, string type, double wage);
        //utolsó módosítás idejének updatelése, automatikusan, mikor változás történik:
        //projekt hozzáadása, törlése, módosítása, csoport módosítása, meber törlése, hozzáadása < TODO
        void UpdateLastChangeDate(int id, DateTime lastchange);
    }
}