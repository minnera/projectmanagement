﻿using System;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.CGConnTRepos
{
    public class CGConnTListRepository : ListRepository<CGCONNT>, ICGConnTRepository
    {
        public CGConnTListRepository()
        {
            Insert(new CGCONNT()
            {
                CGID = 1,
                CID = 1,
                GID = 1,
                CGPOST = "owner"
            });
        }

        public override CGCONNT GetById(int id)
        {
            return Get(akt => akt.CGID == id).SingleOrDefault();
        }

        public void Modify(int id, string post)
        {
            CGCONNT akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            //fentebb le kell kezelni, hogy a lehetséges post fajtákra lehessen csak módosítani TODO
            if (post != null)
            {
                akt.CGPOST = post;
            }
        }
    }
}