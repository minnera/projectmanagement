﻿using System;
using System.Data.Entity;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.CGConnTRepos
{
    public class CGConnTEFRepository : EFRepository<CGCONNT>, ICGConnTRepository
    {
        public CGConnTEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override CGCONNT GetById(int id)
        {
            return Get(akt => akt.CGID == id).SingleOrDefault();
        }

        public void Modify(int id, string post)
        {
            //kell egy másolat példány
            CGCONNT akt = GetById(id);
            //lekezeljük, ha nem létezik a megadott id-jű elem
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            //módosítjuk az új adatokra, ha azok nem null-ok
            //fentebb le kell kezelni, hogy a lehetséges post fajtákra lehessen csak módosítani TODO
            if (post != null)
            {
                akt.CGPOST = post;
            }
            //elmentjük a változásokat
            context.SaveChanges();
        }

    }
}