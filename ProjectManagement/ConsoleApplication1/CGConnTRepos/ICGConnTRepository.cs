﻿using Data;
using Repository.GenericRepos;

namespace Repository.CGConnTRepos
{
    public interface ICGConnTRepository : IRepository<CGCONNT>
    {
        void Modify(int id, string post);
    }
}