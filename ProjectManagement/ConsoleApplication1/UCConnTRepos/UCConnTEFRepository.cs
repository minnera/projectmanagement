﻿using System;
using System.Data.Entity;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.UCConnTRepos
{
    public class UCConnTEFRepository : EFRepository<UCCONNT>, IUCConnTRepository
    {
        public UCConnTEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override UCCONNT GetById(int id)
        {
            return Get(akt => akt.UCID == id).SingleOrDefault();
        }

        public void Modify(int id, string job)
        {
            //kell egy másolat példány
            UCCONNT akt = GetById(id);
            //lekezeljük, ha nem létezik a megadott id-jű elem
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            //módosítjuk az új adatokra, ha azok nem null-ok
            //fentebb le kell kezelni, hogy a lehetséges job fajtákra lehessen csak módosítani TODO
            if (job != null)
            {
                akt.UCJOB = job;
            }
            //elmentjük a változásokat
            context.SaveChanges();
        }
    }
}