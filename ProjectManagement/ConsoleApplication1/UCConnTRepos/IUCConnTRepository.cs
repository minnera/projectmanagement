﻿using Data;
using Repository.GenericRepos;

namespace Repository.UCConnTRepos
{
    public interface IUCConnTRepository : IRepository<UCCONNT>
    {
        void Modify(int id, string job);
    }
}