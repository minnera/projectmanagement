﻿using System;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.UCConnTRepos
{
    public class UCConnTListRepository : ListRepository<UCCONNT>, IUCConnTRepository
    {
        public UCConnTListRepository()
        {
            Insert(new UCCONNT()
            {
                UCID = 1,
                UID = 1,
                CID = 2,
                UCJOB = "értékesítő"
            });

            Insert(new UCCONNT()
            {
                UCID = 2,
                UID = 2,
                CID = 1,
                UCJOB = "panaszkezelő"
            });

            Insert(new UCCONNT()
            {
                UCID = 3,
                UID = 1,
                CID = 1,
                UCJOB = "tanácsadó"
            });
        }

        public override UCCONNT GetById(int id)
        {
            return Get(akt => akt.UCID == id).SingleOrDefault();
        }

        public void Modify(int id, string job)
        {
            UCCONNT akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            //fentebb le kell kezelni, hogy a lehetséges job fajtákra lehessen csak módosítani TODO
            if (job != null)
            {
                akt.UCJOB = job;
            }
        }
    }
}