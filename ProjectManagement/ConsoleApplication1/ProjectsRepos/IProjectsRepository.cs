﻿using System;
using Data;
using Repository.GenericRepos;

namespace Repository.ProjectsRepos
{
    public interface IProjectsRepository : IRepository<PROJECTS>
    {
        //a groupban a projekt módosításra engedélyt kapott tagok tudnják azt módosítani egy külön edit-lapon
        void Modify(int id, string name, bool isarchived, bool ispinned, bool isdone, DateTime? deadline, int priority, string comment);

        //ha bármi változás van a projekttel ez meghívódik
        void UpdateLastModifyDate(int id, DateTime lastmodify);

        //profit változás, ha elkészül feladat
        void ModifyProfit(int id, double profit);
    }
}