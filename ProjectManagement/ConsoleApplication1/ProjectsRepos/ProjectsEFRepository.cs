﻿using System;
using System.Data.Entity;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.ProjectsRepos
{
    public class ProjectsEFRepository : EFRepository<PROJECTS>, IProjectsRepository
    {
        public ProjectsEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override PROJECTS GetById(int id)
        {
            return Get(akt => akt.PID == id).SingleOrDefault();
        }

        public void Modify(int id, string name, bool isarchived, bool ispinned, bool isdone, DateTime? deadline, int priority,
            string comment)
        {
            PROJECTS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            if (name != null)
            {
                akt.PNAME = name;
            }
            akt.PISARCHIVED = isarchived;
            akt.PISPINNED = ispinned;
            akt.PISDONE = isdone;
            akt.PDEADLINE = deadline;
            akt.PPRIORITY = priority;
            if (comment != null)
            {
                akt.PCOMMENT = comment;
            }
            context.SaveChanges();
        }

        public void UpdateLastModifyDate(int id, DateTime lastmodify)
        {
            PROJECTS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            akt.PLASTMODIFY = lastmodify;
            context.SaveChanges();
        }

        public void ModifyProfit(int id, double profit)
        {
            PROJECTS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            akt.PPROFIT = profit;
            context.SaveChanges();
        }
    }
}