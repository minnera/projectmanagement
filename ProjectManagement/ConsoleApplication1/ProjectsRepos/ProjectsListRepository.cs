﻿using System;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.ProjectsRepos
{
    public class ProjectsListRepository : ListRepository<PROJECTS>, IProjectsRepository
    {
        public ProjectsListRepository()
        {
            Insert(new PROJECTS()
            {
                PID = 1,
                PNAME = "Macerás ügyfél #298",
                P_CID_CUSTOMER = 1,
                P_GID_MAKER = 1,
                PISARCHIVED = false,
                PISPINNED = true,
                PISDONE = false,
                PDEADLINE = new DateTime(2017, 06, 08),
                PPRIORITY = 4,
                PRECORDED = new DateTime(2017, 06, 02),
                PPROFIT = 0,
                PCOMMENT = "Nem olyan csomagot kapott, mint amit kért.",
                PLASTMODIFY = DateTime.Now
            });

            Insert(new PROJECTS()
            {
                PID = 2,
                PNAME = "Kocsi karbantartás",
                P_CID_CUSTOMER = null,
                P_GID_MAKER = 2,
                PISARCHIVED = false,
                PISPINNED = false,
                PISDONE = false,
                PDEADLINE = new DateTime(2017, 09, 01),
                PPRIORITY = 2,
                PRECORDED = new DateTime(2017, 06, 01),
                PPROFIT = 0,
                PCOMMENT = "Már a visszapillantó is lóg.",
                PLASTMODIFY = DateTime.Now
            });

            Insert(new PROJECTS()
            {
                PID = 3,
                PNAME = "Macska gondozás",
                P_CID_CUSTOMER = null,
                P_GID_MAKER = 2,
                PISARCHIVED = false,
                PISPINNED = true,
                PISDONE = false,
                PDEADLINE = null,
                PPRIORITY = 3,
                PRECORDED = new DateTime(2017, 01, 02),
                PPROFIT = 0,
                PCOMMENT = "Miau",
                PLASTMODIFY = DateTime.Now
            });

            Insert(new PROJECTS()
            {
                PID = 4,
                PNAME = "Sim kártya csere",
                P_CID_CUSTOMER = 1,
                P_GID_MAKER = 1,
                PISARCHIVED = false,
                PISPINNED = false,
                PISDONE = true,
                PDEADLINE = new DateTime(2017, 06, 01),
                PPRIORITY = 3,
                PRECORDED = new DateTime(2017, 05, 15),
                PPROFIT = 2000,
                PCOMMENT = "Tönkrement a régi sim.",
                PLASTMODIFY = DateTime.Now
            });

            Insert(new PROJECTS()
            {
                PID = 5,
                PNAME = "Gyerek szoba kialakítása",
                P_CID_CUSTOMER = null,
                P_GID_MAKER = 2,
                PISARCHIVED = false,
                PISPINNED = true,
                PISDONE = false,
                PDEADLINE = null,
                PPRIORITY = 3,
                PRECORDED = new DateTime(2017, 01, 02),
                PPROFIT = 0,
                PCOMMENT = "A mennyezet legyen ég színű.",
                PLASTMODIFY = DateTime.Now
            });
        }

        public override PROJECTS GetById(int id)
        {
            return Get(akt => akt.PID == id).SingleOrDefault();
        }

        public void Modify(int id, string name, bool isarchived, bool ispinned, bool isdone, DateTime? deadline, int priority,
            string comment)
        {
            PROJECTS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            if (name != null)
            {
                akt.PNAME = name;
            }
            akt.PISARCHIVED = isarchived;
            akt.PISPINNED = ispinned;
            akt.PISDONE = isdone;
            akt.PDEADLINE = deadline;
            akt.PPRIORITY = priority;
            if (comment != null)
            {
                akt.PCOMMENT = comment;
            }
        }

        public void UpdateLastModifyDate(int id, DateTime lastmodify)
        {
            PROJECTS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            akt.PLASTMODIFY = lastmodify;
        }

        public void ModifyProfit(int id, double profit)
        {
            PROJECTS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            akt.PPROFIT = profit;
        }
    }
}