﻿using System.ComponentModel;
using Data;
using Repository.GenericRepos;

namespace Repository.TasksRepos
{
    public interface ITasksRepository : IRepository<TASKS>
    {
        //akinek van hozzá joga a csoportban az ezeket tudja módosítani
        void Modify(int id, string name, bool isdone, bool isarchived, bool ispinned, double expenses, string comment, int? requirement);
        //mikor valaki leadja, a nevére veszi
        void ModifyUserID(int id, int userid);
    }
}