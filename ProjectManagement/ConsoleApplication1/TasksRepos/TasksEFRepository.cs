﻿using System;
using System.Data.Entity;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.TasksRepos
{
    public class TasksEFRepository : EFRepository<TASKS>, ITasksRepository
    {
        public TasksEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override TASKS GetById(int id)
        {
            return Get(akt => akt.TID == id).SingleOrDefault();
        }

        public void Modify(int id, string name, bool isdone, bool isarchived, bool ispinned, double expenses, string comment,
            int? requirement)
        {
            TASKS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            if (name != null)
            {
                akt.TNAME = name;
            }
            akt.TISDONE = isdone;
            akt.TISARCHIVED = isarchived;
            akt.TISPINNED = ispinned;
            akt.TEXPENSES = expenses;
            if (comment != null)
            {
                akt.TCOMMENT = comment;
            }
            akt.TREQUIREMENT = requirement;
            context.SaveChanges();
        }

        public void ModifyUserID(int id, int userid)
        {
            TASKS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            akt.T_UID = userid;
            context.SaveChanges();
        }
    }
}