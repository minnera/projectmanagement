﻿using System;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.TasksRepos
{
    public class TasksListRepository : ListRepository<TASKS>, ITasksRepository
    {
        public TasksListRepository()
        {
            Insert(new TASKS()
            {
                TID = 1,
                TNAME = "Kocsi mosás",
                T_PID = 2,
                T_UID = 2,
                TISDONE = true,
                TISARCHIVED = false,
                TISPINNED = false,
                TEXPENSES = 0,
                TCOMMENT = "ezt a feladatot kell elvégezni",
                TREQUIREMENT = 2
            });

            Insert(new TASKS()
            {
                TID = 2,
                TNAME = "Visszapillantó tükör csere",
                T_PID = 2,
                T_UID = 1,
                TISDONE = false,
                TISARCHIVED = false,
                TISPINNED = false,
                TEXPENSES = 30000,
                TCOMMENT = "ezt a feladatot kell elvégezni",
                TREQUIREMENT = null
            });

            Insert(new TASKS()
            {
                TID = 3,
                TNAME = "Bőrhuzat az ülésre",
                T_PID = 2,
                T_UID = 1,
                TISDONE = false,
                TISARCHIVED = false,
                TISPINNED = false,
                TEXPENSES = 30000,
                TCOMMENT = "",
                TREQUIREMENT = null
            });

            Insert(new TASKS()
            {
                TID = 4,
                TNAME = "Kipufogó csere",
                T_PID = 2,
                T_UID = 1,
                TISDONE = false,
                TISARCHIVED = false,
                TISPINNED = false,
                TEXPENSES = 30000,
                TCOMMENT = "",
                TREQUIREMENT = null
            });

            Insert(new TASKS()
            {
                TID = 5,
                TNAME = "Új illatosító a kocsiba",
                T_PID = 2,
                T_UID = 1,
                TISDONE = false,
                TISARCHIVED = false,
                TISPINNED = false,
                TEXPENSES = 30000,
                TCOMMENT = "",
                TREQUIREMENT = null
            });

            Insert(new TASKS()
            {
                TID = 6,
                TNAME = "Kiégett ködlámpa csere",
                T_PID = 2,
                T_UID = 1,
                TISDONE = false,
                TISARCHIVED = false,
                TISPINNED = false,
                TEXPENSES = 30000,
                TCOMMENT = "",
                TREQUIREMENT = null
            });

            Insert(new TASKS()
            {
                TID = 7,
                TNAME = "GPS tartót kölcsönkérni",
                T_PID = 2,
                T_UID = 1,
                TISDONE = false,
                TISARCHIVED = false,
                TISPINNED = false,
                TEXPENSES = 30000,
                TCOMMENT = "",
                TREQUIREMENT = null
            });

            Insert(new TASKS()
            {
                TID = 8,
                TNAME = "Ablakmosó folyadék csere",
                T_PID = 2,
                T_UID = 1,
                TISDONE = false,
                TISARCHIVED = false,
                TISPINNED = false,
                TEXPENSES = 30000,
                TCOMMENT = "",
                TREQUIREMENT = null
            });
        }

        public override TASKS GetById(int id)
        {
            return Get(akt => akt.TID == id).SingleOrDefault();
        }

        public void Modify(int id, string name, bool isdone, bool isarchived, bool ispinned, double expenses, string comment,
            int? requirement)
        {
            TASKS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            if (name != null)
            {
                akt.TNAME = name;
            }
            akt.TISDONE = isdone;
            akt.TISARCHIVED = isarchived;
            akt.TISPINNED = ispinned;
            akt.TEXPENSES = expenses;
            if (comment != null)
            {
                akt.TCOMMENT = comment;
            }
            akt.TREQUIREMENT = requirement;
        }

        public void ModifyUserID(int id, int userid)
        {
            TASKS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            akt.T_UID = userid;
        }
    }
}