﻿using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.UPConnTRepos
{
    public class UPConnTListRepository : ListRepository<UPCONNT>, IUPConnTRepository
    {
        public UPConnTListRepository()
        {
            Insert(new UPCONNT()
            {
                UPID = 1,
                UID = 1,
                PID = 2
            });

            Insert(new UPCONNT()
            {
                UPID = 2,
                UID = 1,
                PID = 3
            });

            Insert(new UPCONNT()
            {
                UPID = 3,
                UID = 2,
                PID = 1
            });

            Insert(new UPCONNT()
            {
                UPID = 4,
                UID = 2,
                PID = 3
            });

            Insert(new UPCONNT()
            {
                UPID = 5,
                UID = 2,
                PID = 4
            });
        }

        public override UPCONNT GetById(int id)
        {
            return Get(akt => akt.UPID == id).SingleOrDefault();
        }
    }
}