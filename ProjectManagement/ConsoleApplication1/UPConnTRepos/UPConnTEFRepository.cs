﻿using System;
using System.Data.Entity;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.UPConnTRepos
{
    public class UPConnTEFRepository : EFRepository<UPCONNT>, IUPConnTRepository
    {
        public UPConnTEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override UPCONNT GetById(int id)
        {
            return Get(akt => akt.UPID == id).SingleOrDefault();
        }

        //public void Modify(int id, int uid, int pid)
        //{
        //    //kell egy másolat példány
        //    UPCONNT akt = GetById(id);
        //    //lekezeljük, ha nem létezik a megadott id-jű elem
        //    if (akt == null)
        //    {
        //        throw new ArgumentException("A megadott id-val nem létezik elem.");
        //    }
        //    //módosítjuk az új adatokra, ha azok nem null-ok
        //    //most nincs olyan adat, ami lehetne null
        //    akt.UID = uid;
        //    akt.PID = pid;

        //    //elmentjük a változásokat
        //    context.SaveChanges();
        //}
    }
}