﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Repository.GenericRepos
{
    public interface IRepository<TEntity> : IDisposable
        where TEntity : class
    {
        //TEntity = a tábla neve, amivel dolgozunk

        //táblába táblaelemet beilleszt
        void Insert(TEntity newentity);

        //táblából táblaelemet töröl id alapján
        void Delete(int id);

        //adott táblaelemet (ha létezik a táblában) törli a táblából
        void Delete(TEntity oldentity);

        //táblából visszaadja az megadott id-jú táblaelemet
        TEntity GetById(int id);

        //a táblának az összes elemét visszaadja egy IQueryable tömbként
        IQueryable<TEntity> GetAll();

        //megadott expression (kifejezés) alapján visszaadja az elemeket egy IQueryable tömbként
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition);
    }
}