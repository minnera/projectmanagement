﻿using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;

namespace Repository.GenericRepos
{
    public abstract class EFRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        //ősosztály a tároló alapjának
        protected DbContext context;

        public EFRepository(DbContext newcontext)
        {
            context = newcontext;
        }

        public void Insert(TEntity newentity)
        {
            context.Set<TEntity>().Add(newentity);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            TEntity oldentity = GetById(id);
            if (oldentity == null)
            {
                throw new ArgumentException("NO DATA");
            }
            Delete(oldentity);
        }

        public void Delete(TEntity oldentity)
        {
            context.Set<TEntity>().Remove(oldentity);
            context.SaveChanges();
        }

        //ez kell, de nem használjuk
        public void Dispose()
        {
            context.Dispose();
        }

        //ezt táblafüggetlenül nem csináljuk még meg, mert bonyolult
        public abstract TEntity GetById(int id);

        public IQueryable<TEntity> GetAll()
        {
            return context.Set<TEntity>();
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition)
        {
            return GetAll().Where(condition);
        }
    }
}