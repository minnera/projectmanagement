﻿using Data;
using Repository.GenericRepos;

namespace Repository.CompaniesRepos
{
    public interface ICompaniesRepository : IRepository<COMPANIES>
    {
        //jelszó változtatás
        void ModifyPassword(int id, string password);
        //a cég értékének változtatása (fentebb autómatikusa havonta frissítjük ezt megadott képlet alapján)
        void ModifyPrice(int id, double price);
    }
}