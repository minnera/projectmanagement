﻿using System;
using System.Linq;
using System.Security.Cryptography;
using Data;
using Repository.GenericRepos;

namespace Repository.CompaniesRepos
{
    public class CompaniesListRepository : ListRepository<COMPANIES>, ICompaniesRepository
    {
        public CompaniesListRepository()
        {
            Insert(new COMPANIES()
            {
                CID = 1,
                CNAME = "telekom",
                CPASSWORD = "-570195425", //"feketebillentyuzet"
                CDATE = DateTime.Now,
                CFULLNAME = "Magyar Telekom Nyrt.",
                CINDUSTRY = "távközlés",
                CPRICE = 36400000
            });

            Insert(new COMPANIES()
            {
                CID = 2,
                CNAME = "otpbank",
                CPASSWORD = "-1336227569", //"mattmonitorkepernyo"
                CDATE = new DateTime(2000, 01, 11),
                CFULLNAME = "OTP Bank Nyrt.",
                CINDUSTRY = "bankolás",
                CPRICE = 636321000
            });
        }

        public override COMPANIES GetById(int id)
        {
            return Get(akt => akt.CID == id).SingleOrDefault();
        }

        public void ModifyPassword(int id, string password)
        {
            COMPANIES akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            if (password != null)
            {
                //összefűzzök a jelszót, valami random szöveget
                string salt = "projectmanagement";
                string output = password + salt;
                SHA256 sha = SHA256Managed.Create();
                byte[] hashValue = System.Text.Encoding.UTF8.GetBytes(output);
                hashValue = sha.ComputeHash(hashValue);
                output = String.Join(String.Empty, Array.ConvertAll(hashValue, x => x.ToString("X2")));
                akt.CPASSWORD = output;
                //https://stackoverflow.com/a/311382/3079988
            }
        }

        public void ModifyPrice(int id, double price)
        {
            COMPANIES akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            akt.CPRICE = price;
        }
    }
}