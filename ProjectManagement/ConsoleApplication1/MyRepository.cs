﻿using Repository.CGConnTRepos;
using Repository.CompaniesRepos;
using Repository.GroupsRepos;
using Repository.ProjectsRepos;
using Repository.TasksRepos;
using Repository.UCConnTRepos;
using Repository.UGConnTRepos;
using Repository.UPConnTRepos;
using Repository.UsersRepos;

namespace Repository
{
    public class MyRepository
    {
        //Dependency Injection : design pattern
        public ICompaniesRepository compRepo { get; private set; }
        public IGroupsRepository groupRepo { get; private set; }
        public IProjectsRepository projectRepo { get; private set; }
        public ITasksRepository taskRepo { get; private set; }
        public IUCConnTRepository ucctRepo { get; private set; }
        public IUGConnTRepository ugctRepo { get; private set; }
        public IUPConnTRepository upctRepo { get; private set; }
        public IUsersRepository usersRepo { get; private set; }
        public ICGConnTRepository cgctRepo { get; private set; }

        public MyRepository(ICompaniesRepository cr, IGroupsRepository gr, IProjectsRepository pr, ITasksRepository tr,
            IUCConnTRepository ucr, IUGConnTRepository ugr, IUPConnTRepository upr, IUsersRepository ur, ICGConnTRepository cgr)
        {
            compRepo = cr;
            groupRepo = gr;
            projectRepo = pr;
            taskRepo = tr;
            ucctRepo = ucr;
            ugctRepo = ugr;
            upctRepo = upr;
            usersRepo = ur;
            cgctRepo = cgr;
        }
    }
}