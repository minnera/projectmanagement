﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic.BL_Objects;
using Data;
using Repository;
using Repository.CGConnTRepos;
using Repository.CompaniesRepos;
using Repository.GroupsRepos;
using Repository.ProjectsRepos;
using Repository.TasksRepos;
using Repository.UCConnTRepos;
using Repository.UGConnTRepos;
using Repository.UPConnTRepos;
using Repository.UsersRepos;

namespace BusinessLogic
{
    public class FakeLogicExtra : ILogicExtra
    {
        private MyRepository Repo;

        public FakeLogicExtra()
        {
            //xxListRepository-k a fakelogic-hoz használt, általunk készített feltöltött listák.
            CompaniesListRepository cr = new CompaniesListRepository();
            GroupsListRepository gr = new GroupsListRepository();
            ProjectsListRepository pr = new ProjectsListRepository();
            TasksListRepository tr = new TasksListRepository();
            UCConnTListRepository ucr = new UCConnTListRepository();
            UGConnTListRepository ugr = new UGConnTListRepository();
            UPConnTListRepository upr = new UPConnTListRepository();
            UsersListRepository ur = new UsersListRepository();
            CGConnTListRepository cgr = new CGConnTListRepository();

            Repo = new MyRepository(cr, gr, pr, tr, ucr, ugr, upr, ur, cgr);
        }
    }
}
