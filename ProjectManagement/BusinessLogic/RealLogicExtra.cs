﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BusinessLogic.BL_Objects;
using Data;
using Repository;
using Repository.CGConnTRepos;
using Repository.CompaniesRepos;
using Repository.GroupsRepos;
using Repository.ProjectsRepos;
using Repository.TasksRepos;
using Repository.UCConnTRepos;
using Repository.UGConnTRepos;
using Repository.UPConnTRepos;
using Repository.UsersRepos;

namespace BusinessLogic
{
    public class RealLogicExtra : ILogicExtra
    {
        private MyRepository Repo;

        public RealLogicExtra()
        {
            ProjectManagerEntities3 PME = new ProjectManagerEntities3();
            CompaniesEFRepository cr = new CompaniesEFRepository(PME);
            GroupsEFRepository gr = new GroupsEFRepository(PME);
            ProjectsEFRepository pr = new ProjectsEFRepository(PME);
            TasksEFRepository tr = new TasksEFRepository(PME);
            UCConnTEFRepository ucr = new UCConnTEFRepository(PME);
            UGConnTEFRepository ugr = new UGConnTEFRepository(PME);
            UPConnTEFRepository upr = new UPConnTEFRepository(PME);
            UsersEFRepository ur = new UsersEFRepository(PME);
            CGConnTEFRepository cgr = new CGConnTEFRepository(PME);

            Repo = new MyRepository(cr, gr, pr, tr, ucr, ugr, upr, ur, cgr);
        }

        public RealLogicExtra(MyRepository newRepo)
        {
            Repo = newRepo;
        }
    }
}
