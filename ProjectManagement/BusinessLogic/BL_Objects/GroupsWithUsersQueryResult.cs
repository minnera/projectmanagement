﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.BL_Objects
{
    public class GroupsWithUsersQueryResult
    {
        public string Groupname { set; get; }
        public string Usernames { set; get; }
    }
}
