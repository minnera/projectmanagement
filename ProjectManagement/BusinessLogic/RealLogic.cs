﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using BusinessLogic.BL_Objects;
using Data;
using Repository;
using Repository.CGConnTRepos;
using Repository.CompaniesRepos;
using Repository.GroupsRepos;
using Repository.ProjectsRepos;
using Repository.TasksRepos;
using Repository.UCConnTRepos;
using Repository.UGConnTRepos;
using Repository.UPConnTRepos;
using Repository.UsersRepos;

namespace BusinessLogic
{
    public class RealLogic : ILogic
    {
        private MyRepository Repo;

        public RealLogic()
        {
            ProjectManagerEntities3 PME = new ProjectManagerEntities3();
            CompaniesEFRepository cr = new CompaniesEFRepository(PME);
            GroupsEFRepository gr = new GroupsEFRepository(PME);
            ProjectsEFRepository pr = new ProjectsEFRepository(PME);
            TasksEFRepository tr = new TasksEFRepository(PME);
            UCConnTEFRepository ucr = new UCConnTEFRepository(PME);
            UGConnTEFRepository ugr = new UGConnTEFRepository(PME);
            UPConnTEFRepository upr = new UPConnTEFRepository(PME);
            UsersEFRepository ur = new UsersEFRepository(PME);
            CGConnTEFRepository cgr = new CGConnTEFRepository(PME);

            Repo = new MyRepository(cr, gr, pr, tr, ucr, ugr, upr, ur, cgr);
        }

        public RealLogic(MyRepository newRepo)
        {
            Repo = newRepo;
        }

        //--------------------------------------------------------------------------

        //teszteléshez táblák feltöltése
        public void AddExampleData()
        {
            //TODO
            //AddCompany(new);
        }

        public IQueryable<UsersWithCompaniesQueryResult> GetUsersWithCompanies()
        {
            //query itt, realban, itt csak return list valami

            var q1 = from users in Repo.usersRepo.GetAll()
                join cons in Repo.ucctRepo.GetAll() on users.UID equals cons.UID
                join comps in Repo.compRepo.GetAll() on cons.CID equals comps.CID
                select new UsersWithCompaniesQueryResult()
                {
                    Username = users.UFULLNAME,
                    Companies = comps.CFULLNAME
                };
            //var q2 =
            //    from cons in Repo.ucctRepo.GetAll()
            //    group cons by cons.UID into g
            //    from users in Repo.usersRepo.GetAll()
            //    join  on users.UID equals cons.UID
            //    join comps in Repo.compRepo.GetAll() on cons.CID equals comps.CID
            //    select new UsersWithCompaniesQueryResult()
            //    {
            //        Username = users.UFULLNAME,
            //        Companies = comps.CFULLNAME
            //    };
            return q1;
        }

        public IQueryable<GroupsWithUsersQueryResult> GetGroupsWithUsers()
        {
            var q1 = from groups in Repo.groupRepo.GetAll()
                     join cons in Repo.ugctRepo.GetAll() on groups.GID equals cons.GID
                     join users in Repo.usersRepo.GetAll() on cons.UID equals users.UID
                     select new GroupsWithUsersQueryResult()
                     {
                         Groupname = groups.GNAME,
                         Usernames = users.UFULLNAME
                     };
            return q1;
        }

        public IQueryable<PROJECTS> GetUsersProjectsUnderProcess(int id)
        {
            var q1 = from projects in Repo.projectRepo.GetAll()
                join cons in Repo.upctRepo.GetAll() on projects.PID equals cons.PID
                join users in Repo.usersRepo.GetAll() on cons.UID equals users.UID
                where users.UID == id && !projects.PISDONE
                select projects;
            return q1;
        }

        public IQueryable<PROJECTS> GetUsersFinishedProjects(int id)
        {
            var q1 = from projects in Repo.projectRepo.GetAll()
                join cons in Repo.upctRepo.GetAll() on projects.PID equals cons.PID
                join users in Repo.usersRepo.GetAll() on cons.UID equals users.UID
                where users.UID == id && projects.PISDONE
                select projects;
            return q1;
        }

        public IQueryable<GROUPS> GetUsersGroups(int id)
        {
            var q1 = from groups in Repo.groupRepo.GetAll()
                join cons in Repo.ugctRepo.GetAll() on groups.GID equals cons.GID
                join users in Repo.usersRepo.GetAll() on cons.UID equals users.UID
                where users.UID == id
                select groups;
            return q1;
        }

        public IQueryable<TASKS> GetUsersTasks(int id)
        {
            var q1 = from tasks in Repo.taskRepo.GetAll()
                where tasks.T_UID == id
                select tasks;
            return q1;
        }

        public IQueryable<USERS> GetCompanysUsers(int id)
        {
            var q1 = from users in Repo.usersRepo.GetAll()
                join cons in Repo.ucctRepo.GetAll() on users.UID equals cons.UID
                join companies in Repo.compRepo.GetAll() on cons.CID equals companies.CID
                where companies.CID == id
                select users;
            return q1;
        }

        public IQueryable<GROUPS> GetCompanysGroups(int id)
        {
            var q1 = from groups in Repo.groupRepo.GetAll()
                join cons in Repo.cgctRepo.GetAll() on groups.GID equals cons.GID
                join companies in Repo.compRepo.GetAll() on cons.CID equals companies.CID
                where companies.CID == id
                select groups;
            return q1;
        }

        public IQueryable<PROJECTS> GetGroupsProjects(int id)
        {
            var q1 = from projects in Repo.projectRepo.GetAll()
                where projects.P_GID_MAKER == id
                select projects;
            return q1;
        }

        public IQueryable<PROJECTS> GetGroupsProjectsUnderProcess(int id)
        {
            var q1 = from projects in Repo.projectRepo.GetAll()
                where projects.P_GID_MAKER == id && !projects.PISDONE
                select projects;
            return q1;
        }

        public IQueryable<PROJECTS> GetGroupsFinishedProjects(int id)
        {
            var q1 = from projects in Repo.projectRepo.GetAll()
                where projects.P_GID_MAKER == id && projects.PISDONE
                select projects;
            return q1;
        }

        public IQueryable<USERS> GetGroupsUsers(int id)
        {
            var q1 = from users in Repo.usersRepo.GetAll()
                join cons in Repo.ugctRepo.GetAll() on users.UID equals cons.UID
                join groups in Repo.groupRepo.GetAll() on cons.GID equals groups.GID
                where groups.GID == id
                select users;
            return q1;
        }

        public IQueryable<TASKS> GetProjectsTasks(int id)
        {
            var q1 = from tasks in Repo.taskRepo.GetAll()
                     where tasks.T_PID == id
                     select tasks;
            return q1;
        }

        public IQueryable<COMPANIES> GetCompanies()
        {
            return Repo.compRepo.GetAll();
        }

        public IQueryable<GROUPS> GetGroups()
        {
            return Repo.groupRepo.GetAll();
        }

        //valszeg nem fogjuk használni
        public IQueryable<PROJECTS> GetProjects()
        {
            return Repo.projectRepo.GetAll();
        }

        //valszeg nem fogjuk használni
        public IQueryable<TASKS> GetTasks()
        {
            return Repo.taskRepo.GetAll();
        }

        public IQueryable<USERS> GetUsers()
        {
            return Repo.usersRepo.GetAll();
        }

        public IQueryable<UPCONNT> GetUserProjectsConnections()
        {
            return Repo.upctRepo.GetAll();
        }

        public IQueryable<CGCONNT> GetCompanyGroupConnections()
        {
            return Repo.cgctRepo.GetAll();
        }

        public IQueryable<UCCONNT> GetUserCompanyConnections()
        {
            return Repo.ucctRepo.GetAll();
        }

        public IQueryable<UGCONNT> GetUserGroupConnections()
        {
            return Repo.ugctRepo.GetAll();
        }

        public COMPANIES GetOneCompany(int id)
        {
            return Repo.compRepo.GetById(id);
        }

        public GROUPS GetOneGroup(int id)
        {
            return Repo.groupRepo.GetById(id);
        }

        public PROJECTS GetOneProject(int id)
        {
            return Repo.projectRepo.GetById(id);
        }

        public TASKS GetOneTask(int id)
        {
            return Repo.taskRepo.GetById(id);
        }

        public USERS GetOneUser(int id)
        {
            return Repo.usersRepo.GetById(id);
        }

        public CGCONNT GetOneCompanyGroupConnection(int id)
        {
            return Repo.cgctRepo.GetById(id);
        }

        public UCCONNT GetOneUserCompanyConnection(int id)
        {
            return Repo.ucctRepo.GetById(id);
        }

        public UGCONNT GetOneUserGroupConnection(int id)
        {
            return Repo.ugctRepo.GetById(id);
        }

        public UPCONNT GetOneUserProjectConnection(int id)
        {
            return Repo.upctRepo.GetById(id);
        }

        //alap elem hozzáadások az adatbázishoz

        public void AddCompany(COMPANIES newComp)
        {
            Repo.compRepo.Insert(newComp);
        }

        public void AddUser(USERS newUser)
        {
            Repo.usersRepo.Insert(newUser);
        }

        public void AddGroup(GROUPS newGroup)
        {
            Repo.groupRepo.Insert(newGroup);
        }

        public void AddProject(PROJECTS newProject)
        {
            Repo.projectRepo.Insert(newProject);
        }

        public void AddTask(TASKS newTask)
        {
            Repo.taskRepo.Insert(newTask);
        }

        public void AddCompanyGroupConnection(CGCONNT newConn)
        {
            Repo.cgctRepo.Insert(newConn);
        }

        public void AddUserCompanyConnection(UCCONNT newConn)
        {
            Repo.ucctRepo.Insert(newConn);
        }

        public void AddUserGroupConnection(UGCONNT newConn)
        {
            Repo.ugctRepo.Insert(newConn);
        }

        public void AddUserProjectConnection(UPCONNT newConn)
        {
            Repo.upctRepo.Insert(newConn);
        }

        //konkrét hozzáadó függvények

        //company-ba új user hozzáadása
        public void AddUserForCompany(int userid, int compid, string job)
        {
            var oneUser = Repo.usersRepo.GetById(userid);
            var oneComp = Repo.compRepo.GetById(compid);
            UCCONNT newConn = new UCCONNT()
            {
                USERS = oneUser,
                COMPANIES = oneComp,
                UID = oneUser.UID,
                CID = oneComp.CID,
                UCJOB = job,
                UCID = GetNextUserCompanyConnectionId()
            };

            Repo.ucctRepo.Insert(newConn);
        }

        //user regisztráció
        public void AddUser(string username, string password, string gender, DateTime? birthdate, string fullname)
        {
            if (username == null)
            {
                throw new ArgumentException("Nem lehet \"null\" a username.");
            }
            if (password == null)
            {
                throw new ArgumentException("Nem lehet \"null\" a password.");
            }
            if (gender != "female" && gender != "male")
            {
                throw new ArgumentException("A user neme vagy 'female' vagy 'male' lehet csak!");
            }
            //összefűzzök a jelszót, valami random szöveget és a usernevet
            string salt = "projectmanagement";
            string output = password + salt + username;
            SHA256 sha = SHA256Managed.Create();
            byte[] hashValue = System.Text.Encoding.UTF8.GetBytes(output);
            hashValue = sha.ComputeHash(hashValue);
            password = String.Join(String.Empty, Array.ConvertAll(hashValue, x => x.ToString("X2")));
            
            //https://stackoverflow.com/a/311382/3079988
            USERS newuser = new USERS()
            {
                UID = GetNextUserId(),
                UNAME = username,
                UPASSWORD = password,
                UGENDER = gender,
                UBDATE = birthdate,
                UFULLNAME = fullname,
                UTOTALSALARY = 0
            };

            Repo.usersRepo.Insert(newuser);
        }

        //company regisztráció
        public void AddCompany(string companyname, string password, DateTime startdate, string fullname, string industry, double price)
        {
            if(companyname == null)
            {
                throw new ArgumentException("Nem lehet \"null\" a companyname.");
            }
            if (password == null)
            {
                throw new ArgumentException("Nem lehet \"null\" a password.");
            }
            //összefűzzök a jelszót, valami random szöveget
            string salt = "projectmanagement";
            string output = password + salt;
            SHA256 sha = SHA256Managed.Create();
            byte[] hashValue = System.Text.Encoding.UTF8.GetBytes(output);
            hashValue = sha.ComputeHash(hashValue);
            password = String.Join(String.Empty, Array.ConvertAll(hashValue, x => x.ToString("X2")));

            //https://stackoverflow.com/a/311382/3079988

            COMPANIES newcomp = new COMPANIES()
            {
                CID = GetNextCompanyId(),
                CNAME = companyname,
                CPASSWORD = password,
                CDATE = startdate,
                CFULLNAME = fullname,
                CINDUSTRY = industry,
                CPRICE = price
            };

            Repo.compRepo.Insert(newcomp);
        }

        //új company group létrehozása
        public void AddCompanyGroup(int compid, string groupname, string visibility, string type, double wage)
        {
            //egyik sem lehet null
            if (groupname == null)
            {
                throw new ArgumentException("Nem lehet \"null\" a name.");
            }
            //lehet: company, private, public
            if (visibility != "company" && visibility != "private" && visibility != "public")
            {
                throw new ArgumentException("A visibility csak ez lehet: company, private, public.");
            }
            //lehet: business, circle, private
            if (type != "business" && type != "circle" && type != "private")
            {
                throw new ArgumentException("A type csak ez lehet: business, circle, private.");
            }
            int gid = GetNextGroupId();
            GROUPS newgroup = new GROUPS()
            {
                GID = gid,
                GNAME = groupname,
                GVISIBILITY = visibility,
                GDATE = DateTime.Now,
                GTYPE = type,
                GWAGE = wage,
                GLASTCHANGE = DateTime.Now
            };

            Repo.groupRepo.Insert(newgroup);

            //ezután a company-group kapcsolatot hozzáadjuk

            CGCONNT newConn = new CGCONNT()
            {
                CGID = GetNextCompanyGroupConnectionId(),
                CGPOST = "owner",
                CID = compid,
                COMPANIES = Repo.compRepo.GetById(compid),
                GID = gid,
                GROUPS = Repo.groupRepo.GetById(gid)
            };

            Repo.cgctRepo.Insert(newConn);
        }

        //új user group létrehozása
        public void AddUserGroup(int userid, string groupname, string visibility, string type, double wage)
        {
            //egyik sem lehet null
            if (groupname == null)
            {
                throw new ArgumentException("Nem lehet \"null\" a name.");
            }
            //lehet: company, private, public
            if (visibility != "company" && visibility != "private" && visibility != "public")
            {
                throw new ArgumentException("A visibility csak ez lehet: company, private, public.");
            }
            //lehet: business, circle, private
            if (type != "business" && type != "circle" && type != "private")
            {
                throw new ArgumentException("A type csak ez lehet: business, circle, private.");
            }
            int gid = GetNextGroupId();
            GROUPS newgroup = new GROUPS()
            {
                GID = gid,
                GNAME = groupname,
                GVISIBILITY = visibility,
                GDATE = DateTime.Now,
                GTYPE = type,
                GWAGE = wage,
                GLASTCHANGE = DateTime.Now
            };

            Repo.groupRepo.Insert(newgroup);

            //ezután a user-group kapcsolatot hozzáadjuk

            UGCONNT newConn = new UGCONNT()
            {
                UGID = GetNextCompanyGroupConnectionId(),
                UGPOST = "owner",
                UID = userid,
                USERS = Repo.usersRepo.GetById(userid),
                GID = gid,
                GROUPS = Repo.groupRepo.GetById(gid)
            };

            Repo.ugctRepo.Insert(newConn);
        }

        //új projekt felvétele groupba
        public void AddProjectToGroup(int groupid, string projectname, int? companyid, DateTime? deadline, int priority, string comment)
        {
            //csak a comment és a deadline lehet null (és a companyid)
            if (projectname == null)
            {
                throw new ArgumentException("Nem lehet \"null\" a name.");
            }

            //priority csak 0-9 között lehet
            if (priority < 0 && priority > 9)
            {
                throw new ArgumentException("A priority csak 0-9 közötti szám lehet.");
            }

            COMPANIES newcomp;
            if (companyid == null)
            {
                newcomp = null;
            }
            else
            {
                newcomp = Repo.compRepo.GetById((int)companyid);
            }
            
            PROJECTS newproject = new PROJECTS()
            {
                COMPANIES = newcomp,
                GROUPS = Repo.groupRepo.GetById(groupid),
                P_CID_CUSTOMER = companyid,
                P_GID_MAKER = groupid,
                PCOMMENT = comment,
                PDEADLINE = deadline,
                PID = GetNextProjectId(),
                PISARCHIVED = false,
                PISDONE = false,
                PISPINNED = false,
                PNAME = projectname,
                PPRIORITY = priority,
                PRECORDED = DateTime.Now,
                PPROFIT = 0,
                PLASTMODIFY = DateTime.Now
            };

            Repo.projectRepo.Insert(newproject);
        }

        //új task felvétele projecthez
        public void AddTaskToProject(int projectid, string taskname, string comment, int? requirement)
        {
            if (taskname == null)
            {
                throw new ArgumentException("Nem lehet \"null\" a name.");
            }
            TASKS requirementtask;
            if (requirement == null)
            {
                requirementtask = null;
            }
            else
            {
                requirementtask = Repo.taskRepo.GetById((int)requirement);
            }
            TASKS newtask = new TASKS()
            {
                PROJECTS = Repo.projectRepo.GetById(projectid),
                T_PID = projectid,
                T_UID = null,
                TASKS2 = requirementtask,
                TCOMMENT = comment,
                TEXPENSES = 0,
                TID = GetNextTaskId(),
                TISPINNED = false,
                TISDONE = false,
                TISARCHIVED = false,
                TREQUIREMENT = requirement,
                TNAME = taskname
            };

            Repo.taskRepo.Insert(newtask);
        }

        //----------------------------------------------------------------------------------------------------

        //törlések

        public void DelUser(int id)
        {
            //legelőször a groupjainak törlése!!!! vagyis ha a UGPOST == "owner"
            //és a többi user-group kapcsolat törlése
            List<UGCONNT> copy3 = new List<UGCONNT>(Repo.ugctRepo.GetAll());
            foreach (UGCONNT connection in copy3)
            {
                if (connection.UID == id)
                {
                    Repo.ugctRepo.Delete(connection);
                    if (connection.UGPOST == "owner")
                    {
                        DelGroup(connection.GID);
                    }
                }
            }
            // törölni kell a user-company, user-project kapcsolatokat
            List<UPCONNT> copy = new List<UPCONNT>(Repo.upctRepo.GetAll());
            foreach (UPCONNT connection in copy)
            {
                if (connection.UID == id)
                {
                    Repo.upctRepo.Delete(connection);
                }
            }
            List<UCCONNT> copy2 = new List<UCCONNT>(Repo.ucctRepo.GetAll());
            foreach (UCCONNT connection in copy2)
            {
                if (connection.UID == id)
                {
                    Repo.ucctRepo.Delete(connection);
                }
            }

            Repo.usersRepo.Delete(id);
        }

        public void DelGroup(int id)
        {
            //először törölni kell vagy a user-group vagy a company-group kapcsolatokat
            List<UGCONNT> copy = new List<UGCONNT>(Repo.ugctRepo.GetAll());
            foreach (UGCONNT connection in copy)
            {
                if (connection.GID == id)
                {
                    Repo.ugctRepo.Delete(connection);
                }
            }
            List<CGCONNT> copy2 = new List<CGCONNT>(Repo.cgctRepo.GetAll());
            foreach (CGCONNT connection in copy2)
            {
                if (connection.GID == id)
                {
                    Repo.cgctRepo.Delete(connection);
                }
            }
            //és törölni kell a projekteket a groupban
            List<PROJECTS> copy3 = new List<PROJECTS>(Repo.projectRepo.GetAll());
            foreach (PROJECTS p in copy3)
            {
                if (p.P_GID_MAKER == id)
                {
                    DelProject(p.PID);
                    //ez törli a user-project kapcsolatokat is és a taskokat
                }
            }

            //ezután törölhetjük a groupot
            Repo.groupRepo.Delete(id);
        }

        public void DelProject(int id)
        {
            //először törölni kell a user-project connection-öket
            List<UPCONNT> copy = new List<UPCONNT>(Repo.upctRepo.GetAll());
            //copy = Repo.upctRepo.GetAll().ToList();
            foreach (UPCONNT connection in copy)
            {
                if (connection.PID == id)
                {
                    Repo.upctRepo.Delete(connection);
                }
            }
            //törölni kell a taskokat rajta
            List<TASKS> copy2 = new List<TASKS>(Repo.taskRepo.GetAll());
            foreach (TASKS task in copy2)
            {
                if (task.T_PID == id)
                {
                    Repo.taskRepo.Delete(task);
                }
            }
            //akarunk valamit a project költségeivel csinálni valamit?
            //ezután törölhető a project
            Repo.projectRepo.Delete(id);
        }

        public void DelUPConnection(UPCONNT upconnect)
        {
            Repo.upctRepo.Delete(upconnect);
        }

        public void DelTask(int id)
        {
            Repo.taskRepo.Delete(id);
        }

        //--------------------------------------------------------------------------------------------

        //Módisítások

        public void ModifyCompanyPassword(int id, string password)
        {
            Repo.compRepo.ModifyPassword(id, password);
        }

        public void ModifyCompanyPrice(int id, double price)
        {
            Repo.compRepo.ModifyPrice(id, price);
        }

        public void ModifyGroup(int id, string name, string type, double wage)
        {
            Repo.groupRepo.Modify(id, name, type, wage);
        }

        public void UpdateGroupLastChangeDate(int id, DateTime lastchange)
        {
            Repo.groupRepo.UpdateLastChangeDate(id, lastchange);
        }

        public void ModifyProject(int id, string name, bool isarchived, bool ispinned, bool isdone, DateTime? deadline, int priority,
            string comment)
        {
            Repo.projectRepo.Modify(id, name, isarchived, ispinned, isdone, deadline, priority, comment);
        }

        public void ModifyProject(PROJECTS project)
        {
            Repo.projectRepo.Modify(project.PID, project.PNAME, project.PISARCHIVED, project.PISPINNED, project.PISDONE, project.PDEADLINE, project.PPRIORITY, project.PCOMMENT);
        }

        public void UpdateProjectLastModifyDate(int id, DateTime lastmodify)
        {
            Repo.projectRepo.UpdateLastModifyDate(id, lastmodify);
        }

        public void ModifyProjectProfit(int id, double profit)
        {
            Repo.projectRepo.ModifyProfit(id, profit);
        }

        public void ModifyTask(int id, string name, bool isdone, bool isarchived, bool ispinned, double expenses, string comment,
            int? requirement)
        {
            Repo.taskRepo.Modify(id, name, isdone, isarchived, ispinned, expenses, comment, requirement);
        }

        public void ModifyTasK(TASKS task)
        {
            Repo.taskRepo.Modify(task.TID, task.TNAME, task.TISDONE, task.TISARCHIVED, task.TISPINNED, task.TEXPENSES, task.TCOMMENT, task.TREQUIREMENT);
        }

        public void ModifyUserIDInTask(int id, int userid)
        {
            Repo.taskRepo.ModifyUserID(id, userid);
            //ekkor a user-project táblát frissíteni kell!!!! TODO
        }

        public void ModifyJobInCompany(int id, string job)
        {
            Repo.ucctRepo.Modify(id, job);
        }

        public void ModifyPostInUserGroup(int id, string post)
        {
            Repo.ugctRepo.Modify(id, post);
        }

        public void ModifyPostInCompGroup(int id, string post)
        {
            Repo.cgctRepo.Modify(id, post);
        }

        public void ModifyUser(int id, string name, string password, string gender, DateTime? bdate, string fullname)
        {
            Repo.usersRepo.Modify(id, name, password, gender, bdate, fullname);
        }

        public void ModifyUsersTotalSalary(int id, double totalsalary)
        {
            Repo.usersRepo.ModifyTotalSalary(id, totalsalary);
        }

        //--------------------------------------------------------------------------------------------

        //Next ID generálók

        public int GetNextCompanyId()
        {
            if (!Repo.compRepo.GetAll().Any()) return 1;
            return Repo.compRepo.GetAll().Max(x => (int)x.CID) + 1;
        }

        public int GetNextGroupId()
        {
            if (!Repo.groupRepo.GetAll().Any()) return 1;
            return Repo.groupRepo.GetAll().Max(x => (int)x.GID) + 1;
        }

        public int GetNextProjectId()
        {
            if (!Repo.projectRepo.GetAll().Any()) return 1;
            return Repo.projectRepo.GetAll().Max(x => (int)x.PID) + 1;
        }

        public int GetNextTaskId()
        {
            if (!Repo.taskRepo.GetAll().Any()) return 1;
            return Repo.taskRepo.GetAll().Max(x => (int)x.TID) + 1;
        }

        public int GetNextUserId()
        {
            if (!Repo.usersRepo.GetAll().Any()) return 1;
            return Repo.usersRepo.GetAll().Max(x => (int)x.UID) + 1;
        }

        public int GetNextUserCompanyConnectionId()
        {
            if (!Repo.ucctRepo.GetAll().Any()) return 1;
            return Repo.ucctRepo.GetAll().Max(x => (int)x.UCID) + 1;
        }

        public int GetNextUserProjectConnectionId()
        {
            if (!Repo.upctRepo.GetAll().Any()) return 1;
            return Repo.upctRepo.GetAll().Max(x => (int)x.UPID) + 1;
        }

        public int GetNextUserGroupConnectionId()
        {
            if (!Repo.ugctRepo.GetAll().Any()) return 1;
            return Repo.ugctRepo.GetAll().Max(x => (int)x.UGID) + 1;
        }

        public int GetNextCompanyGroupConnectionId()
        {
            if (!Repo.cgctRepo.GetAll().Any()) return 1;
            return Repo.cgctRepo.GetAll().Max(x => (int)x.CGID) + 1;
        }
       
        public bool IsTaskAssignedToAUser(int id)
        {
            return Repo.taskRepo.GetById(id).T_UID != null;
        }

        public bool IsProjectAssignedToAnyUser(int id)
        {
            foreach (UPCONNT connection in Repo.upctRepo.GetAll())
            {
                if (connection.PID == id)
                {
                    return true;
                }
            }
            return false;
        }

        public bool CompanyCheck(string companyusername, string password)
        {
            //összefűzzök a jelszót, valami random szöveget
            string salt = "projectmanagement";
            string output = password + salt;
            SHA256 sha = SHA256Managed.Create();
            byte[] hashValue = System.Text.Encoding.UTF8.GetBytes(output);
            hashValue = sha.ComputeHash(hashValue);
            output = String.Join(String.Empty, Array.ConvertAll(hashValue, x => x.ToString("X2")));

            //https://stackoverflow.com/a/311382/3079988

            foreach (COMPANIES company in Repo.compRepo.GetAll())
            {
                if(company.CNAME == companyusername)
                {
                    if(company.CPASSWORD == output)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        
        public bool UserCheck(string username, string password)
        {
            //összefűzzök a jelszót, valami random szöveget és a usernevet
            string salt = "projectmanagement";
            string output = password + salt + username;
            SHA256 sha = SHA256Managed.Create();
            byte[] hashValue = System.Text.Encoding.UTF8.GetBytes(output);
            hashValue = sha.ComputeHash(hashValue);
            output = String.Join(String.Empty, Array.ConvertAll(hashValue, x => x.ToString("X2")));
            Console.WriteLine(output);
            //https://stackoverflow.com/a/311382/3079988

            foreach (USERS user in Repo.usersRepo.GetAll())
            {
                if (user.UNAME == username)
                {
                    if (user.UPASSWORD == output)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

//TODO szűrni, hogy a max karakterhossz adják meg a string mezőkhöz , ami el tud férni a táblában