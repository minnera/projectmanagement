﻿using System;
using System.Linq;
using BusinessLogic.BL_Objects;
using Data;

namespace BusinessLogic
{
    public interface ILogic
    {
        //A táblák összes elemét visszaadó függvények

        IQueryable<COMPANIES> GetCompanies();
        //net<
        IQueryable<GROUPS> GetGroups();
        //wpf<
        //net<
        IQueryable<PROJECTS> GetProjects();
        //net<
        IQueryable<TASKS> GetTasks();
        //wpf<
        IQueryable<USERS> GetUsers();
        IQueryable<CGCONNT> GetCompanyGroupConnections();
        IQueryable<UCCONNT> GetUserCompanyConnections();
        IQueryable<UGCONNT> GetUserGroupConnections();
        IQueryable<UPCONNT> GetUserProjectsConnections();

        //---------------------------------------------------------

        //bizonyos elemeket visszaadó függvények

        //visszaadja az összes usert, és csak a nevüket jeleníti meg és hogy melyik companiban dolgoznak
        IQueryable<UsersWithCompaniesQueryResult> GetUsersWithCompanies();

        //visszaadja az összes csoportot és a hozzájuk tartozó felhasználókat
        IQueryable<GroupsWithUsersQueryResult> GetGroupsWithUsers();

        //net<
        //usernek folyamatban levő projectjeinek lekérése
        IQueryable<PROJECTS> GetUsersProjectsUnderProcess(int id);

        //usernek befejezett projekteinek lekérése
        IQueryable<PROJECTS> GetUsersFinishedProjects(int id);

        //user csoportjai
        IQueryable<GROUPS> GetUsersGroups(int id);

        //user nevén levő taskok lekérése
        IQueryable<TASKS> GetUsersTasks(int id);

        //egy company embereinek lekérése
        IQueryable<USERS> GetCompanysUsers(int id);

        //company groupjainak lekérése
        IQueryable<GROUPS> GetCompanysGroups(int id);

        //group projectjeinek lekérése
        IQueryable<PROJECTS> GetGroupsProjects(int id);

        //group befejezetlen projectjeinek lekérése
        IQueryable<PROJECTS> GetGroupsProjectsUnderProcess(int id);

        //group befejezett projectjeinek lekérése
        IQueryable<PROJECTS> GetGroupsFinishedProjects(int id);

        //group usereinek lekérése
        IQueryable<USERS> GetGroupsUsers(int id);

        //project taskjainak lekérése
        IQueryable<TASKS> GetProjectsTasks(int id);

        //-------------------------------------------------------

        //egy elemet visszaadó függvények

        //mikor valaki belép company-ként
        COMPANIES GetOneCompany(int id);

        //mikor valaki megnyit egy group-ot
        GROUPS GetOneGroup(int id);

        //net<
        //mikor valaki megnyit egy project-et
        PROJECTS GetOneProject(int id);

        //net<
        //mikor valaki megnyit egy task-ot
        TASKS GetOneTask(int id);

        //mikor valaki user-ként lép be
        USERS GetOneUser(int id);

        //company-group connection elemet ad vissza
        CGCONNT GetOneCompanyGroupConnection(int id);

        //user-company connection elemet ad vissza
        UCCONNT GetOneUserCompanyConnection(int id);

        //user-group connection elemet ad vissza
        UGCONNT GetOneUserGroupConnection(int id);

        //user-project connection elemet ad vissza
        UPCONNT GetOneUserProjectConnection(int id);


        //-----------------------------------------------

        //új elemet hozzáadó függvények, sablonok

        //mikor egy új company regisztrál
        void AddCompany(COMPANIES newComp);

        //mikor egy új user regisztrál
        void AddUser(USERS newUser);

        //mikor valaki létrehoz egy új group-ot
        void AddGroup(GROUPS newGroup);

        //net<
        //mikor valaki létrehoz egy új project-et
        void AddProject(PROJECTS newProject);

        //net<
        //mikor valaki hozzáad egy új task-ot
        void AddTask(TASKS newTask);

        //új company-group connection hozzáadása
        void AddCompanyGroupConnection(CGCONNT newConn);

        //új user-company connection hozzáadása
        void AddUserCompanyConnection(UCCONNT newConn);

        //új user-group connection hozzáadása
        void AddUserGroupConnection(UGCONNT newConn);

        //új user-project connection hozzáadása
        void AddUserProjectConnection(UPCONNT newConn);

        //-------------------------------------

        //egy elemet konkrétan hozzáadó függvények

        //company regisztráció
        void AddCompany(string companyname, string password, DateTime startdate, string fullname, string industry, double price);

        //company-ba új user hozzáadása
        void AddUserForCompany(int userid, int compid, string job);

        //user regisztráció
        void AddUser(string username, string password, string gender, DateTime? birthdate, string fullname);

        //új company group létrehozása
        void AddCompanyGroup(int compid, string groupname, string visibility, string type, double wage);

        //új user group létrehozása
        void AddUserGroup(int userid, string groupname, string visibility, string type, double wage);

        //új projekt felvétele groupba
        void AddProjectToGroup(int groupid, string projectname, int? companyid, DateTime? deadline, int priority, string comment);

        //új task felvétele projecthez
        void AddTaskToProject(int projectid, string taskname, string comment, int? requirement);



        //TÖRLÉSEK

        //egyelőre company-t nem lehet törölni

        //wpf<
        //először a csoportokat töröljük, amiknek a tulajdonosa,
        //utána törölni kell a useren a kapcsolatokat.
        void DelUser(int id);

        //group törlése, csak a tulajdonos tudja?
        void DelGroup(int id);

        //wpd<
        //net<
        //project törlése, előbb törölni kell a projekt-user összekötéseket.
        void DelProject(int id);

        //net<
        //task törlése
        void DelTask(int id);

        void DelUPConnection(UPCONNT upconnect);

        //---------------------------------------------------------

        //modify

        //jelszó változtatás
        void ModifyCompanyPassword(int id, string password);
        //a cég értékének változtatása (fentebb autómatikusa havonta frissítjük ezt megadott képlet alapján)
        void ModifyCompanyPrice(int id, double price);
        //név, láthatóság, típus és bér változtatható
        void ModifyGroup(int id, string name, string type, double wage);
        //utolsó módosítás idejének updatelése, automatikusan, mikor változás történik:
        //projekt hozzáadása, törlése, módosítása, csoport módosítása, meber törlése, hozzáadása < TODO
        void UpdateGroupLastChangeDate(int id, DateTime lastchange);
        //wpf<
        //net<
        //a groupban a projekt módosításra engedélyt kapott tagok tudnják azt módosítani egy külön edit-lapon
        void ModifyProject(int id, string name, bool isarchived, bool ispinned, bool isdone, DateTime? deadline, int priority, string comment);
        void ModifyProject(PROJECTS project);
        //ha bármi változás van a projekttel ez meghívódik
        void UpdateProjectLastModifyDate(int id, DateTime lastmodify);
        //profit változás, ha elkészül feladat
        void ModifyProjectProfit(int id, double profit);
        //net<
        //akinek van hozzá joga a csoportban az ezeket tudja módosítani
        void ModifyTask(int id, string name, bool isdone, bool isarchived, bool ispinned, double expenses, string comment, int? requirement);
        void ModifyTasK(TASKS task);
        //mikor valaki leadja, a nevére veszi
        void ModifyUserIDInTask(int id, int userid);
        //company-n belül egy user job státuszának változtatása
        void ModifyJobInCompany(int id, string job);
        //user group-ban egy user post-jának változtatása (jogosultságok)
        void ModifyPostInUserGroup(int id, string post);
        //company group-ban egy user post-jának változtatása (jogosultságok)
        void ModifyPostInCompGroup(int id, string post);
        //wpf<
        //a user saját maga tudja ezeket változtatni
        void ModifyUser(int id, string name, string password, string gender, DateTime? bdate, string fullname);
        //kifizetéseknél változik
        void ModifyUsersTotalSalary(int id, double totalsalary);

        //következő id (1-esével)
        
        int GetNextCompanyId();
        int GetNextGroupId();
        //wpf<
        //net<
        int GetNextProjectId();
        //net<
        int GetNextTaskId();
        //wpf<
        int GetNextUserId();
        int GetNextUserCompanyConnectionId();
        int GetNextUserProjectConnectionId();
        int GetNextUserGroupConnectionId();
        int GetNextCompanyGroupConnectionId();

        //Egyéb függvények

        //az adott task hozzá van-e rendelve egy userhez?
        bool IsTaskAssignedToAUser(int id);

        //az adott projecthez hozzá van-e rendelve user?
        bool IsProjectAssignedToAnyUser(int id);

        //Company belépés ellenőrzés
        bool CompanyCheck(string companyusername, string password);

        //User belépés ellenőrzés
        bool UserCheck(string username, string password);
    }
}